<?php

namespace Drupal\eme\EmeFieldValueExporter\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * EME field value exporter plugin annotation.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class EmeFieldValueExporter extends Plugin {

  /**
   * List of supported field types.
   *
   * @var string[]
   */
  public array $field_types = [];

}
