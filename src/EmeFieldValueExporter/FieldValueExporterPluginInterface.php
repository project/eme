<?php

declare(strict_types=1);

namespace Drupal\eme\EmeFieldValueExporter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Interface of field value exporter plugins.
 */
interface FieldValueExporterPluginInterface {

  /**
   * Returns the migration process pipeline of the field.
   *
   * @return string|array|array[]
   *   The migration process pipeline of the field.
   */
  public function getProcessPipeline(FieldDefinitionInterface $field_definition);

  /**
   * List of the modules whose are required to evaluate the process pipeline.
   *
   * @return string[]
   *   List of the modules whose are required to evaluate the process pipeline.
   *   For example, if one of the process plugins is provided by 'foo' module,
   *   the plugin class must return ['foo'] (or ['foo:foo']).
   */
  public function getModuleDependencies(): array;

  /**
   * Returns the value of the given field item list.
   *
   * @return mixed
   *   The field value in the given field item list.
   */
  public function getFieldValue(FieldItemListInterface $item_list): mixed;

}
