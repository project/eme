<?php

declare(strict_types=1);

namespace Drupal\eme\EmeFieldValueExporter;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\eme\EmeFieldValueExporter\Annotation\EmeFieldValueExporter;
use Psr\Log\LoggerInterface;

/**
 * Plugin manager of field value exporter plugins.
 */
class FieldValueExporterManager extends DefaultPluginManager implements FieldValueExporterManagerInterface {

  /**
   * The plugin group identifier.
   *
   * Used as cache ID and also in the plugin alter hook.
   *
   * @const string
   */
  const PLUGIN_GROUP_ID = 'eme_field_value_exporter_plugins';

  /**
   * ID of the fallback plugin.
   *
   * @const string
   */
  const FALLBACK_PLUGIN_ID = 'general';

  /**
   * Constructs a FieldValueExporterManager instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    \Traversable $namespaces,
    ModuleHandlerInterface $module_handler,
    CacheBackendInterface $cache_backend,
    protected LoggerInterface $logger,
  ) {
    parent::__construct(
      'Plugin/Eme/FieldValueExporter',
      $namespaces,
      $module_handler,
      FieldValueExporterPluginInterface::class,
      EmeFieldValueExporter::class
    );

    $this->alterInfo(static::PLUGIN_GROUP_ID);
    $this->setCacheBackend($cache_backend, static::PLUGIN_GROUP_ID);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceForFieldDefinition(FieldDefinitionInterface $field_definition): FieldValueExporterPluginInterface {
    $instance = $this->createInstance(
      $this->getPluginIdForFieldType($field_definition->getType())
    );
    assert($instance instanceof FieldValueExporterPluginInterface);
    return $instance;
  }

  /**
   * Returns plugin ID of the exporter plugin that can export the field type.
   *
   * @param string $field_type
   *   The type of the field to export.
   *
   * @return string
   *   The plugin ID of the field value exporter plugin.
   */
  protected function getPluginIdForFieldType(string $field_type): string {
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if (!isset($definition['field_types'])) {
        continue;
      }
      if (!in_array($field_type, (array) $definition['field_types'], TRUE)) {
        continue;
      }
      return $plugin_id;
    }

    return static::FALLBACK_PLUGIN_ID;
  }

}
