<?php

declare(strict_types=1);

namespace Drupal\eme\EmeFieldValueExporter;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Interface of the field value exporter plugin manager.
 */
interface FieldValueExporterManagerInterface {

  /**
   * Returns a field value exporter plugin for the given field definition.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition what should be exported.
   *
   * @return \Drupal\eme\EmeFieldValueExporter\FieldValueExporterPluginInterface
   *   The first field value exporter plugin for the given field definition that
   *   can export the field.
   */
  public function getInstanceForFieldDefinition(FieldDefinitionInterface $field_definition): FieldValueExporterPluginInterface;

}
