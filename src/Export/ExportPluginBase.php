<?php

declare(strict_types=1);

namespace Drupal\eme\Export;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\Variable;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\Exception\FileWriteException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\eme\Component\TemporaryExport;
use Drupal\eme\DependencyDiscoveryInterface;
use Drupal\eme\Eme;
use Drupal\eme\EmeExportable\ExportablePluginInterface;
use Drupal\eme\EmeExportable\ExportablePluginManagerInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\EmeObjectRelation;
use Drupal\eme\ExportException;
use Drupal\eme\Utility\EmeModuleFileUtils;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for entity export plugins.
 */
abstract class ExportPluginBase extends PluginBase implements ExportPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The dependency discovery service.
   *
   * @var \Drupal\eme\DependencyDiscoveryInterface
   */
  protected $dependencyDiscovery;

  /**
   * Entity memory cache.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface
   */
  protected $entityMemoryCache;

  /**
   * The exportable plugin manager.
   *
   * @var \Drupal\eme\EmeExportable\ExportablePluginManagerInterface
   */
  protected $exportablePluginManager;

  /**
   * The logger to use.
   *
   * @var \Psr\Log\LoggerInterface|null
   */
  protected $logger;

  /**
   * Whether translations should be included or not.
   *
   * @var bool
   */
  protected $includeTranslations = TRUE;

  /**
   * The temporary export module being created.
   *
   * @var \Drupal\eme\Component\TemporaryExport
   */
  protected $export;

  /**
   * Constructs an export plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock backend.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_list
   *   The module extension list service.
   * @param \Drupal\eme\DependencyDiscoveryInterface $dependency_discovery
   *   The reference discovery plugin manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $entity_memory_cache
   *   Entity memory cache.
   * @param \Drupal\eme\EmeExportable\ExportablePluginManagerInterface $exportable_plugin_manager
   *   The exportable plugin manager.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, LockBackendInterface $lock, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FileSystemInterface $file_system, ModuleExtensionList $module_list, DependencyDiscoveryInterface $dependency_discovery, MemoryCacheInterface $entity_memory_cache, ExportablePluginManagerInterface $exportable_plugin_manager) {
    $configuration += [
      'types' => [],
      'module' => Eme::getModuleName(),
      'name' => Eme::getModuleHumanName(),
      'id-prefix' => Eme::getDefaultId(),
      'group' => Eme::getDefaultId(),
      'path' => NULL,
    ];
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->lock = $lock;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->fileSystem = $file_system;
    $this->moduleExtensionList = $module_list;
    $this->dependencyDiscovery = $dependency_discovery;
    $this->entityMemoryCache = $entity_memory_cache;
    $this->exportablePluginManager = $exportable_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('lock.persistent'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('file_system'),
      $container->get('extension.list.module'),
      $container->get('eme.reference_discovery'),
      $container->get('entity.memory_cache'),
      $container->get('plugin.manager.eme.exportable')
    );
  }

  /**
   * The ordered tasks which create the migration export.
   *
   * @return string[]
   *   An array of method names which are invoked to complete the export.
   */
  public function tasks(): array {
    return [
      'initializeExport',
      'discoverContentReferences',
      'writeMigrationPlugins',
      'buildModule',
      'finishExport',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(LoggerInterface $logger = NULL): void {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function hasLogger(): bool {
    return !is_null($this->logger);
  }

  /**
   * Initializes the export process.
   *
   * @param \ArrayAccess $context
   *   The batch context.
   */
  protected function initializeExport(\ArrayAccess &$context): void {
    $this->sendMessage($this->t('Initializing the export process.'), $context);
    $this->temporaryExport()->reset();
    $this->doLock();
    $context['finished'] = 1;
  }

  /**
   * Discovers referred content entities as a batch operation.
   *
   * @param \ArrayAccess $context
   *   The batch context.
   */
  protected function discoverContentReferences(\ArrayAccess &$context): void {
    $sandbox = &$context['sandbox'];
    if (!isset($sandbox['objects_to_export'])) {
      $this->sendMessage($this->t('Discovering content references...'), $context);
      $sandbox['objects_to_export'] = [];
      $sandbox['objects_checked'] = [];
      $context['results']['discovered'] = [];
      $sandbox['objects_to_export'] = $this->loadTypes($this->configuration['types']);
      $sandbox['progress'] = 0;
      $sandbox['total'] = count($sandbox['objects_to_export']);
    }

    $this->sendMessage($this->t('Discovering content references: (@processed/@total)', [
      '@processed' => $sandbox['progress'],
      '@total' => $sandbox['total'],
    ]), $context);

    $unchecked = array_diff($sandbox['objects_to_export'], $sandbox['objects_checked']);

    if ($current_object_id = reset($unchecked)) {
      $eme_object = EmeObject::createFromId($current_object_id);
      $sandbox['progress'] += 1;

      $exportable = $this->exportablePluginManager->getInstanceForEmeObject($eme_object);

      if (!$exportable->shouldBeExcluded()) {
        $eme_relation_id = $exportable->getRelation()->getId();

        // Add discovered referenced entities to the export array.
        $direct_dependency_objects = $this->dependencyDiscovery->getDependencies($eme_object);
        $sandbox['objects_to_export'] += array_combine(
          array_keys($direct_dependency_objects),
          array_keys($direct_dependency_objects)
        );
        $context['results']['relations'][$eme_relation_id] = array_merge(
          $context['results']['relations'][$eme_relation_id] ?? [],
          array_reduce(
            $direct_dependency_objects,
            function (array $carry, EmeObject $eme_object) {
              $exportable = $this->exportablePluginManager->getInstanceForEmeObject($eme_object);
              $relation_id = $exportable->getRelation()->getId();
              $carry[$relation_id] = $relation_id;
              return $carry;
            },
            []
          )
        );

        $reverse_dependency_objects = $this->dependencyDiscovery->getReverseDependencies($eme_object);
        $sandbox['objects_to_export'] += array_combine(
          array_keys($reverse_dependency_objects),
          array_keys($reverse_dependency_objects)
        );

        foreach ($reverse_dependency_objects as $reverse_dependency_object) {
          $reverse_relation_id = $this->exportablePluginManager->getInstanceForEmeObject($reverse_dependency_object)->getRelation()->getId();
          $context['results']['relations'][$reverse_relation_id] = array_unique(
            array_merge(
              $context['results']['relations'][$reverse_relation_id] ?? [],
              [$eme_relation_id => $eme_relation_id]
            )
          );
        }

        $context['results']['discovered'] += [$current_object_id => $current_object_id];
      }

      $sandbox['total'] = count($sandbox['objects_to_export']);
      // Add entity to the results.
      $sandbox['objects_checked'][$current_object_id] = $current_object_id;
    }

    if ($sandbox['progress'] < $sandbox['total']) {
      $context['finished'] = $sandbox['progress'] / $sandbox['total'];
      $this->flushEntityMemoryCache($sandbox['progress']);
    }
    else {
      natsort($context['results']['discovered']);
      $context['finished'] = 1;
      $this->flushEntityMemoryCache();
    }
  }

  /**
   * Generates the migration plugin definitions.
   *
   * @param \ArrayAccess $context
   *   The batch context.
   */
  protected function writeMigrationPlugins(\ArrayAccess &$context): void {
    $sandbox = &$context['sandbox'];
    if (!isset($sandbox['total'])) {
      $this->sendMessage($this->t('Generating the migration plugin definitions.'), $context);
      $sandbox['plugins_to_write'] = array_keys($context['results']['relations'] ?? []);
      $sandbox['progress'] = 0;
      $sandbox['total'] = count($sandbox['plugins_to_write']);
    }

    $this->sendMessage($this->t('Generating the migration plugin definitions: (@processed/@total).', [
      '@processed' => $sandbox['progress'],
      '@total' => $sandbox['total'],
    ]), $context);

    // Create the migration plugin definition (a Yaml).
    if ($current_relation_id = array_shift($sandbox['plugins_to_write'])) {
      $exportable = $this->exportablePluginManager->getInstanceFromRelation(
        EmeObjectRelation::createFromId($current_relation_id)
      );
      $migration_id = $this->getMigrationId(
        $exportable->getEntityTypeId(),
        $exportable->getBundle()
      );

      $plugin_definition = $this->createMigrationPluginDefinition($exportable, $context['results']);

      $this->temporaryExport()->addFileWithContent(
        Eme::MIGRATION_DIR . "/$migration_id.yml",
        Yaml::encode($plugin_definition)
      );

      $context['results']['migration_ids'][] = $migration_id;
      $sandbox['progress'] += 1;
    }

    if ($sandbox['progress'] < $sandbox['total']) {
      $context['finished'] = $sandbox['progress'] / $sandbox['total'];
      $this->flushEntityMemoryCache($sandbox['progress']);
    }
    else {
      $context['finished'] = 1;
      $this->flushEntityMemoryCache();
    }
  }

  /**
   * Builds the exported module, meaning its info and module file (if needed).
   *
   * @param \ArrayAccess $context
   *   The batch context.
   */
  protected function buildModule(\ArrayAccess &$context): void {
    $this->sendMessage($this->t('Finalize the module.'), $context);
    $module = $this->configuration['module'];
    $module_list = $this->moduleExtensionList->reset()->getList();
    $module_exists = array_key_exists($module, $module_list);
    $migration_ids = $context['results']['migration_ids'] ?? [];
    natsort($migration_ids);
    if (is_array($this->configuration['types'])) {
      sort($this->configuration['types']);
    }
    $types = is_array($this->configuration['types'])
      ? array_values($this->configuration['types'])
      : $this->configuration['types'];
    $info_yaml = [
      'name' => $this->configuration['name'],
      'type' => 'module',
      'description' => 'Generated with EME module',
      'core_version_requirement' => '^8.9 || ^9 || ^10 || ^11',
      'scenarios_module' => $module,
      'dependencies' => [],
      'eme_settings' => [],
    ];
    $module_file_content = EmeModuleFileUtils::getBareModuleFile($module);

    if ($module_exists) {
      $module_path = $module_list[$module]->getPath();
      $info_yaml = Yaml::decode(file_get_contents(implode('/', [
        $module_path,
        "$module.info.yml",
      ])));

      // The module file (if it exists) should be kept.
      $module_file_path = implode('/', [
        $module_path,
        "$module.module",
      ]);
      if (file_exists($module_file_path)) {
        $module_file_content = file_get_contents($module_file_path);
      }

      // Let's delete the previous migration definitions.
      foreach ($info_yaml['eme_settings']['migrations'] ?? [] as $migration_id) {
        // Delete data sources.
        $yaml_path = implode('/', [
          $module_path,
          Eme::MIGRATION_DIR,
          "$migration_id.yml",
        ]);

        if (file_exists($yaml_path)) {
          $this->fileSystem->deleteRecursive($yaml_path);
        }
      }
    }

    $extra_dependencies = ['drupal:migrate'];
    foreach (array_keys($context['results']['relations'] ?? []) as $relation_id) {
      $exportable = $this->exportablePluginManager->getInstanceFromRelation(EmeObjectRelation::createFromId($relation_id));
      $extra_dependencies = array_merge(
        $extra_dependencies,
        $exportable->getModuleDependencies()
      );
    }

    $dependencies = array_unique(
      array_merge(
        $info_yaml['dependencies'] ?? [],
        $extra_dependencies
      )
    );
    natsort($dependencies);
    $info_yaml['dependencies'] = array_values($dependencies);
    $info_yaml['eme_settings'] = [
      'plugin' => $this->getPluginId(),
      'migrations' => array_values($migration_ids),
      'types' => $types,
      'id-prefix' => $this->configuration['id-prefix'],
      'group' => $this->configuration['group'],
    ];

    // Add the alterer.
    EmeModuleFileUtils::ensureUseDeclaration(
      "Drupal\\{$module}\\ModuleImplementsAlterer",
      $module_file_content
    );
    EmeModuleFileUtils::ensureFunctionUsedInHook(
      'hook_module_implements_alter',
      ['&$implementations', '$hook'],
      'ModuleImplementsAlterer::alter($1, $2)',
      $module,
      $module_file_content
    );
    $this->temporaryExport()->addFileWithContent(
      'src/ModuleImplementsAlterer.php',
      EmeModuleFileUtils::moduleImplementsAltererClass($module)
    );

    // Add info Yaml and the module file.
    $this->temporaryExport()->addFileWithContent(
      "$module.info.yml",
      Yaml::encode($info_yaml)
    );
    $this->temporaryExport()->addFileWithContent(
      "$module.module",
      $module_file_content
    );

    $context['finished'] = 1;
    $context['results']['redirect'] = empty($this->configuration['path']);
  }

  /**
   * Moves the export to the codebase or creates an archive for downloading.
   *
   * @param \ArrayAccess $context
   *   The batch context.
   */
  protected function finishExport(\ArrayAccess &$context): void {
    if ($destination = $this->configuration['path']) {
      $this->sendMessage($this->t('Copy the export module to codebase.'), $context);
      $module = $this->configuration['module'];
      $module_list = $this->moduleExtensionList->reset()->getList();
      $module_exists = array_key_exists($module, $module_list);
      $module_path = $module_exists
        ? $module_list[$module]->getPath()
        : implode('/', [$destination, $module]);
      if (!$this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
        throw new \RuntimeException(sprintf("Cannot prepare the directory '%s'", $destination));
      }

      // If destination is not absolute and isn't a stream, add DRUPAL_ROOT.
      if (
        !StreamWrapperManager::getScheme($module_path) &&
        strpos($module_path, DIRECTORY_SEPARATOR) !== 0
      ) {
        $module_path = DRUPAL_ROOT . DIRECTORY_SEPARATOR . $module_path;
      }

      if (!$this->temporaryExport()->move($module_path)) {
        throw new FileWriteException(sprintf("Cannot move the temporary archive to Drupal codebase."));
      }
    }
    else {
      $this->sendMessage($this->t('Create the export module archive.'), $context);
      $this->temporaryExport()->createTemporaryArchive();
    }

    $context['finished'] = 1;
    $this->releaseLock();
  }

  /**
   * Loads objects to export based on the specified entity types config.
   *
   * @param string|array $types
   *   The list of the entity type IDs to export, or the "all" string. Latter
   *   means everything should be exported.
   *
   * @return \Drupal\eme\EmeObject[]
   *   List of EME objects to export.
   */
  protected function loadTypes(string|array $types): array {
    if ($types === 'all') {
      $types = array_reduce(
        $this->entityTypeManager->getDefinitions(),
        function (array $carry, EntityTypeInterface $entityType): array {
          if ($entityType instanceof ContentEntityTypeInterface) {
            $carry[] = $entityType->id();
          }
          return $carry;
        },
        [],
      );
    }
    $objects = [];
    foreach ($types as $type) {
      $objects += $this->loadEntitiesByType($type);
    }
    return $objects;
  }

  /**
   * {@inheritdoc}
   */
  final public function executeExportTask(string $export_task, &$context): void {
    // In order for being able to validate an export plugin, steps are only
    // allowed to get an \ArrayAccess instance as parameter.
    // This helper callback translates a Drupal Form API batch context with type
    // 'array' to an \ArrayObject and keeps sync its values with the original
    // batch context.
    $array_access_context = is_array($context)
      ? new \ArrayObject($context)
      : $context;
    try {
      $this->$export_task($array_access_context);
      if (is_array($context)) {
        foreach ($array_access_context as $key => $value) {
          $context[$key] = $value;
        }
      }
      return;
    }
    catch (\Throwable $exception) {
    }

    $this->releaseLock();
    if (!empty($exception)) {
      throw new ExportException(sprintf("Unexpected error while processing %s.", Variable::export($export_task)), 1, $exception);
    }
  }

  /**
   * Entity loader.
   *
   * @param string $entity_type
   *   Content Entity Type which entities should be loaded.
   *
   * @return \Drupal\eme\EmeObject[]
   *   List of content entity IDs.
   */
  protected function loadEntitiesByType($entity_type): array {
    // Try each combination.
    $typePieces = explode(':', $entity_type, 2);
    $entity_type = array_shift($typePieces);
    // Second piece might be a bundle, or an entity ID.
    $bundle_or_id = $typePieces[0] ?? NULL;

    if (in_array($entity_type, Eme::getExcludedTypes(), TRUE)) {
      return [];
    }
    if (
      $bundle_or_id &&
      in_array("$entity_type:$bundle_or_id", Eme::getExcludedTypes(), TRUE)
    ) {
      return [];
    }
    if (!($definition = $this->entityTypeManager->getDefinition($entity_type, FALSE))) {
      return [];
    }
    if (!$definition instanceof ContentEntityTypeInterface) {
      return [];
    }

    $entity_storage = $this->entityTypeManager->getStorage($entity_type);
    assert($entity_storage instanceof ContentEntityStorageInterface);
    $bundle = NULL;
    if ($bundle_or_id) {
      // Determine if the second piece refers to an id or a bundle.
      if (is_null($entity_storage->load($bundle_or_id))) {
        $bundle = $bundle_or_id;
      }
      else {
        $entity_ids = [$bundle_or_id];
      }
    }

    if (empty($entity_ids)) {
      $entity_ids_query = $entity_storage->getQuery()->accessCheck(FALSE);
      if ($bundle) {
        $entity_ids_query->condition($definition->getKey('bundle'), $bundle);
      }
      $entity_ids = $entity_ids_query->execute();
    }

    $eme_object_ids = array_reduce(
      $entity_storage->loadMultiple($entity_ids),
      function (array $carry, $entity) {
        $eme_object = EmeObject::createFromContentEntity($entity);
        $carry[$eme_object->getId()] = $eme_object->getId();
        return $carry;
      },
      []
    );
    return $eme_object_ids;
  }

  /**
   * Creates an array that represents a migration plugin definition.
   *
   * @param \Drupal\eme\EmeExportable\ExportablePluginInterface $exportable
   *   The exportable plugin instance.
   * @param array $results
   *   The results of the previously executed export tasks.
   *
   * @return array
   *   A migration plugin definition as array.
   */
  protected function createMigrationPluginDefinition(ExportablePluginInterface $exportable, array $results): array {
    return [
      'label' => implode(' ', array_filter([
        'Import',
        $exportable->getEntityTypeId(),
        $exportable->getBundle(),
      ])),
      'migration_tags' => [
        'Drupal ' . explode('.', \Drupal::VERSION)[0],
        'Content',
        $this->configuration['name'],
        $this->configuration['group'],
      ],
      'migration_group' => $this->configuration['group'],
      'id' => $this->getMigrationIdFromExportable($exportable),
      'source' => [],
      'process' => $exportable->getProcessPipelines(),
      'destination' => $exportable->getDestination($this->includeTranslations),
      'migration_dependencies' => $this->getEvaluatedMigrationDependencies($exportable, $results['relations'][$exportable->getRelation()->getId()] ?? []),
    ];
  }

  /**
   * Returns the list of evaluated migration dependencies.
   *
   * @param \Drupal\eme\EmeExportable\ExportablePluginInterface $exportable
   *   The exportable plugin instance.
   * @param array $dependency_relation_ids
   *   Migration IDs of the dependencies, keyed by the dependency type (which
   *   can be 'optional' or 'required').
   *
   * @return string[][]
   *   The migration dependencies, keyed by the dependency type.
   */
  protected function getEvaluatedMigrationDependencies(ExportablePluginInterface $exportable, array $dependency_relation_ids): array {
    $dependencies = array_reduce(
      $dependency_relation_ids,
      function (array $carry, string $id) use ($exportable) {
        $dep_exportable = $this->exportablePluginManager->getInstanceFromRelation(EmeObjectRelation::createFromId($id));
        $dependency_migration_id = $this->getMigrationIdFromExportable($dep_exportable);
        // Migration shouldn't depend on itself.
        if ($dependency_migration_id === $this->getMigrationIdFromExportable($exportable)) {
          return $carry;
        }

        $dependency_type = $exportable->dependencyIsRequiredForMigration($dep_exportable->getEntityTypeId(), $dep_exportable->getBundle())
          ? 'required'
          : 'optional';
        $carry[$dependency_type] = array_unique(array_merge(
          $carry[$dependency_type] ?? [],
          [$this->getMigrationIdFromExportable($dep_exportable)]
        ));
        return $carry;
      },
      []
    );

    foreach (array_keys($dependencies) as $dependency_type) {
      natsort($dependencies[$dependency_type]);
      $dependencies[$dependency_type] = array_values($dependencies[$dependency_type]);
    }
    return $dependencies;
  }

  /**
   * Returns the ID of a content entity migration plugin definition.
   *
   * @param string $entity_type_id
   *   The entity type ID of the entity, e.g. "node".
   * @param string|null $bundle
   *   The bundle ID of the entity, e.g. "article".
   *
   * @return string
   *   The ID of a content entity migration plugin definition.
   */
  private function getMigrationId(string $entity_type_id, string $bundle = NULL): string {
    return implode('_', array_filter([
      $this->configuration['id-prefix'],
      $entity_type_id,
      $bundle,
    ]));
  }

  /**
   * Returns the ID of a content entity migration plugin definition.
   *
   * @return string
   *   The ID of a content entity migration plugin definition.
   */
  private function getMigrationIdFromExportable(ExportablePluginInterface $exportable): string {
    return implode('_', array_filter([
      $this->configuration['id-prefix'],
      $exportable->getEntityTypeId(),
      $exportable->getBundle(),
    ]));
  }

  /**
   * Sends a message to the current "UI".
   *
   * @param string|\Drupal\Core\StringTranslation\TranslatableMarkup $message
   *   The message.
   * @param array|\ArrayAccess $context
   *   A batch context.
   * @param bool $forced
   *   Whether the message should be sent not only for the first round.
   */
  protected function sendMessage($message, &$context, bool $forced = FALSE): void {
    $no_message_was_set_before = empty($context['sandbox']['message_set']);
    $context['sandbox']['message_set'] = TRUE;
    if ($this->hasLogger() && ($no_message_was_set_before || $forced)) {
      $this->logger->notice($message);
    }

    if ($no_message_was_set_before || !(class_exists("DrushBatchContext") && $context instanceof \DrushBatchContext)) {
      $context['message'] = $message;
    }
  }

  /**
   * Locks the export process.
   */
  protected function doLock(): void {
    if (!$this->lock->acquire(Eme::LOCK_NAME)) {
      throw new ExportException('An another process is already exporting content.');
    }
  }

  /**
   * Releases the lock of the export process.
   */
  protected function releaseLock(): void {
    $this->lock->release(Eme::LOCK_NAME);
  }

  /**
   * Determines if an export is already running.
   *
   * @return bool
   *   TRUE if an export is already running, FALSE if not.
   */
  public function alreadyProcessing(): bool {
    return !$this->lock->lockMayBeAvailable(Eme::LOCK_NAME);
  }

  /**
   * Drops static entity memory cache (per 10 iteration).
   *
   * @param int|null $progress
   *   The current progress. If this is NULL, then the cache will be dropped
   *   without any condition.
   */
  protected function flushEntityMemoryCache(int $progress = NULL): void {
    if (
      $progress === NULL ||
      fmod((float) $progress, 10.0) < 0.01
    ) {
      $this->entityMemoryCache->deleteAll();
    }
  }

  /**
   * Returns the actual export module.
   *
   * @return \Drupal\eme\Component\TemporaryExport
   *   The export module.
   */
  protected function temporaryExport() {
    if ($this->export === NULL) {
      $this->export = new TemporaryExport($this->fileSystem->getTempDirectory());
    }

    return $this->export;
  }

}
