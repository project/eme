<?php

declare(strict_types=1);

namespace Drupal\eme;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Identifies an exportable object (in most of the time, a content entity).
 *
 * @internal
 */
class EmeObject implements \Stringable {

  /**
   * Separates ID of the Exportable plugin from the object identifiers.
   *
   * Used to construct the ID of an EmeObject, which is the string
   * representation of the object.
   *
   * @const string
   */
  const ID_SEPARATOR = '|';

  /**
   * ID of the Exportable plugin that can export this object.
   *
   * @var string
   */
  protected string $exportablePluginId;

  /**
   * The identifiers of the EmeObject.
   *
   * Stores the IDs of the exported object (usually a content entity ID, maybe
   * the revision ID or the language code too), but there is no limitation what
   * or how much data can be stored here. The goal is to identify objects which
   * should be exported.
   *
   * @var array
   */
  protected array $objectIds;

  /**
   * Constructs a new EmeObject instance.
   *
   * @param string $exportable_plugin_id
   *   ID of the Exportable plugin that can export the object (usually a content
   *   entity) identified by this EmeObject.
   * @param array $object_ids
   *   List of IDs that identify the object to export (e.g. a content entity).
   *   For example content entity ID, maybe revision ID or / and language code.
   */
  public function __construct(string $exportable_plugin_id, array $object_ids) {
    $this->exportablePluginId = $exportable_plugin_id;
    // Ensure all IDs are standardized strings.
    $this->objectIds = array_map(
      function ($id): string {
        if (is_string($id) || is_int($id)) {
          return (string) $id;
        }
        throw new \LogicException(sprintf(
          "Object IDs must be strings or integers, got %s",
          gettype($id),
        ));
      },
      array_values($object_ids),
    );
  }

  /**
   * Creates an EmeObject from the string representation.
   *
   * @param string $id
   *   The string representation of an EmeObject.
   *
   * @return self
   *   The EmeObject instance.
   */
  public static function createFromId(string $id): self {
    [$plugin_id, $encoded_object_ids] = explode(self::ID_SEPARATOR, $id);
    return new EmeObject(
      $plugin_id,
      Json::decode($encoded_object_ids)
    );
  }

  /**
   * Creates an EmeObject from a content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity instance.
   *
   * @return \Drupal\eme\EmeObject
   *   An Eme Object instance.
   */
  public static function createFromContentEntity(ContentEntityInterface $entity): self {
    $object_ids = [$entity->id()];
    if ($revision_id = $entity->getRevisionId()) {
      $object_ids[] = $revision_id;
    }
    return new EmeObject($entity->getEntityTypeId(), $object_ids);
  }

  /**
   * The ID of the Exportable plugin that can export the object.
   *
   * @return string
   *   ID of the exportable plugin that can export the object.
   */
  public function getPluginId(): string {
    return $this->exportablePluginId;
  }

  /**
   * Returns the IDs of the exportable object.
   *
   * @return array
   *   The identifiers of the exportable object.
   */
  public function getObjectIds(): array {
    return $this->objectIds;
  }

  /**
   * Returns the string representation of the EmeObject instance.
   *
   * @return string
   *   String representation of the EmeObject instance.
   */
  public function getId(): string {
    return $this->exportablePluginId . self::ID_SEPARATOR . Json::encode($this->objectIds);
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return $this->getId();
  }

}
