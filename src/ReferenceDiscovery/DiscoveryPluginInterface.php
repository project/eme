<?php

declare(strict_types=1);

namespace Drupal\eme\ReferenceDiscovery;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\eme\EmeObject;

/**
 * Interface for reference discovery plugins.
 */
interface DiscoveryPluginInterface extends PluginInspectionInterface {

  /**
   * Returns content entities which are referenced by the given entity.
   *
   * @param \Drupal\eme\EmeObject $emeObject
   *   The EME object to check for references.
   *
   * @return \Drupal\eme\EmeObject[]
   *   EmeObject representation of the direct dependencies.
   */
  public function fetchReferences(EmeObject $emeObject): array;

  /**
   * Returns entities which are related to and depend on the given entity.
   *
   * @param \Drupal\eme\EmeObject $emeObject
   *   The EME object which might have reverse dependencies.
   *
   * @return \Drupal\eme\EmeObject[]
   *   EmeObject representation of the things which are related to and depend on
   *   the given thing.
   */
  public function fetchReverseReferences(EmeObject $emeObject): array;

}
