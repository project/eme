<?php

declare(strict_types=1);

namespace Drupal\eme\ReferenceDiscovery;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\eme\EmeObject;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class of reference discovery plugins.
 */
abstract class DiscoveryPluginBase extends PluginBase implements DiscoveryPluginInterface, ContainerFactoryPluginInterface {

  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityFieldManagerInterface $entityFieldManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fetchReferences(EmeObject $emeObject): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function fetchReverseReferences(EmeObject $emeObject): array {
    return [];
  }

}
