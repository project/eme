<?php

namespace Drupal\eme;

/**
 * Object that stores relation of a migration dependency.
 *
 * @internal
 */
class EmeObjectRelation implements \Stringable {

  /**
   * Separates ID of the Exportable plugin from the relation object identifiers.
   *
   * Used to construct the ID of an EmeObjectRelation, which is the string
   * representation of the object.
   *
   * @const string
   */
  const RELATION_ID_SEPARATOR = '|';

  /**
   * ID of the Exportable plugin that can export object with this relation.
   *
   * @var string
   */
  protected string $exportablePluginId;

  /**
   * Qualifiers of the relation.
   *
   * This is usually the bundle of the content entity.
   *
   * @var string[]
   */
  protected array $ids = [];

  /**
   * Constructs an EmeObjectRelation instance.
   */
  public function __construct() {
    $arguments = func_get_args();
    foreach ($arguments as $argument) {
      if (gettype($argument) !== 'string') {
        throw new \InvalidArgumentException();
      }
    }
    $this->exportablePluginId = array_shift($arguments);
    $this->ids = $arguments;
  }

  /**
   * Returns the string representation of the EmeObjectRelation instance.
   *
   * @return string
   *   String representation of the EmeObject instance.
   */
  public function getId(): string {
    return implode(self::RELATION_ID_SEPARATOR, array_merge(
      [$this->exportablePluginId],
      $this->ids
    ));
  }

  /**
   * Creates an EmeObjectRelation from a string representation.
   *
   * @param string $id_string
   *   The string representation of an EmeObjectRelation.
   *
   * @return self
   *   The EmeObjectRelation instance.
   */
  public static function createFromId(string $id_string): self {
    return new EmeObjectRelation(
      ...explode(self::RELATION_ID_SEPARATOR, $id_string)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return $this->getId();
  }

}
