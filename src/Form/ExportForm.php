<?php

namespace Drupal\eme\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\Url;
use Drupal\eme\Access\TemporarySchemeAccessCheck;
use Drupal\eme\Eme;
use Drupal\eme\Export\ExportPluginManager;
use Drupal\eme\InterfaceAwareExportBatchRunner;
use Drupal\eme\Utility\EmeCollectionUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for starting a Content Entity to Migrations batch.
 */
class ExportForm extends ExportFormBase {

  protected const MACHINE_NAME_PATTERN = '[a-z][a-z0-9_]*';

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * Construct an ExportForm instance.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_list
   *   The module extension list service.
   * @param \Drupal\eme\InterfaceAwareExportBatchRunner $batch_runner
   *   The export batch runner.
   * @param \Drupal\eme\Export\ExportPluginManager $export_plugin_manager
   *   The export plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   *   The entity bundle info service.
   */
  public function __construct(
    ModuleExtensionList $module_list,
    InterfaceAwareExportBatchRunner $batch_runner,
    ExportPluginManager $export_plugin_manager,
    EntityTypeManagerInterface $entity_type_manager,
    StreamWrapperManagerInterface $stream_wrapper_manager,
    RendererInterface $renderer,
    EntityTypeBundleInfoInterface $entity_bundle_info,
  ) {
    parent::__construct($module_list, $batch_runner, $export_plugin_manager);
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfo = $entity_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module'),
      $container->get('eme.batch_runner'),
      $container->get('eme.export_plugin_manager'),
      $container->get('entity_type.manager'),
      $container->get('stream_wrapper_manager'),
      $container->get('renderer'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eme_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type = NULL) {
    $temporary_stream_access = (new TemporarySchemeAccessCheck($this->streamWrapperManager))->access();
    if (!$temporary_stream_access->isAllowed()) {
      $form['info'] = [
        '#type' => 'item',
        '#markup' => $this->t(
          "The temporary file directory isn't accessible. You must configure it for being able to export content. See the <a href=\":system-file-settings-link\">File system configuration form</a> for further info.",
          [
            ':system-file-settings-link' => Url::fromRoute('system.file_system_settings')->toString(),
          ]
        ),
      ];
      return $form;
    }

    $form['#tree'] = FALSE;
    $form['row'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['layout-row', 'clearfix']],
    ];
    $form['row']['col_first'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['layout-column', 'layout-column--half'],
      ],
    ];
    $form['row']['col_last'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['layout-column', 'layout-column--half'],
      ],
    ];

    $default_id = Eme::getDefaultId();
    $export_options = array_map(function ($definition) {
      $label_content = [
        'main' => [
          '#markup' => $definition['label'],
        ],
        'description' => [
          '#prefix' => '<br>',
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $definition['description'],
          '#attributes' => ['class' => ['description']],
        ],
      ];
      return $this->renderer->renderRoot($label_content);
    }, $this->exportPluginManager->getDefinitions());

    // If there is only one export plugin, then users don't need to pick one.
    $plugin_options_form_item = [
      '#type' => 'radios',
      '#title' => $this->t('Type of the export'),
      '#options' => $export_options,
      '#required' => TRUE,
    ];
    if (count($export_options) === 1) {
      $plugin_keys = array_keys($export_options);
      $plugin_options_form_item = [
        '#type' => 'value',
        '#value' => reset($plugin_keys),
      ];
    }

    // Export module config, metadata and structure.
    $form['row']['col_first']['plugin'] = $plugin_options_form_item;
    $form['row']['col_first']['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Export ID'),
      '#placeholder' => $default_id,
      '#description' => $this->t("A general ID. Used if one of module name, migration prefix and migration group isn't defined. By default, migration group and migration ID prefix will be this ID. Module name will be <code>[ID]_content</code>."),
      '#required' => FALSE,
      '#machine_name' => [
        'exists' => [get_class($this), 'machineNameExists'],
      ],
      '#attributes' => [
        'data-eme-export-id' => 'eme-id',
        'pattern' => self::MACHINE_NAME_PATTERN,
      ],
      '#attached' => [
        'library' => ['eme/export-id'],
        'drupalSettings' => [
          'emeExport' => [
            'eme-id' => [
              'source' => [
                '[data-drupal-selector="edit-id"]' => 'replacement',
              ],
              'destination' => [
                '[data-drupal-selector="edit-module"]' => Eme::getModuleName('(replacement)'),
                '[data-drupal-selector="edit-name"]' => [Eme::getModuleHumanName('(replacement)')],
                '[data-drupal-selector="edit-prefix"]' => '(replacement)',
                '[data-drupal-selector="edit-group"]' => '(replacement)',
              ],
            ],
          ],
        ],
      ],
    ];
    $form['row']['col_first']['module'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Module name'),
      '#placeholder' => $default_id . '_content',
      '#description' => $this->t('The <em>machine name</em> of the generated module.'),
      '#required' => FALSE,
      '#machine_name' => [
        'exists' => [get_class($this), 'machineNameExists'],
      ],
      '#attributes' => [
        'pattern' => self::MACHINE_NAME_PATTERN,
      ],
    ];
    $form['row']['col_first']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Module human name'),
      '#placeholder' => Eme::getModuleHumanName($default_id),
      '#description' => $this->t('The <em>human-readable</em> name of the generated module'),
    ];
    $form['row']['col_first']['prefix'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Migration prefix'),
      '#placeholder' => $default_id,
      '#description' => $this->t('An ID prefix for the generated migration plugin definitions.'),
      '#required' => FALSE,
      '#machine_name' => [
        'exists' => [get_class($this), 'machineNameExists'],
      ],
      '#attributes' => [
        'pattern' => self::MACHINE_NAME_PATTERN,
      ],
    ];
    $form['row']['col_first']['group'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Migration group'),
      '#placeholder' => $default_id,
      '#description' => $this->t('The migration group of generated migration plugin definitions.'),
      '#required' => FALSE,
      '#machine_name' => [
        'exists' => [get_class($this), 'machineNameExists'],
      ],
      '#attributes' => [
        'pattern' => self::MACHINE_NAME_PATTERN,
      ],
    ];

    $form['row']['col_last']['col_title'] = [
      '#type' => 'item',
      '#field_prefix' => '<strong>',
      '#field_suffix' => '</strong>',
      '#markup' => $this->t('Content entities to export'),
    ];

    $form['row']['col_last']['all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export everything'),
      '#return_value' => 'all',
    ];
    $form['row']['col_last']['or'] = [
      '#type' => 'item',
      '#markup' => $this->t('OR'),
      '#attached' => ['library' => ['eme/admin']],
      '#wrapper_attributes' => ['class' => ['eme-horizontal-line']],
      '#states' => [
        'invisible' => [
          ':input[name="all"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['row']['col_last']['types_title'] = [
      '#type' => 'item',
      '#markup' => $this->t('Select which content entities should be exported'),
      '#states' => [
        'invisible' => [
          ':input[name="all"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['row']['col_last']['types'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => ['class' => ['form-boolean-group']],
    ] + $this->getTypesAndBundleElements();

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#op' => 'default',
        '#value' => $this->t('Start export'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $id = empty($form_state->getValue('id'))
      ? Eme::ID
      : $form_state->getValue('id');
    $module_name_to_export = empty($form_state->getValue('module'))
      ? "{$id}_content"
      : $form_state->getValue('module');
    if (in_array($module_name_to_export, $this->discoveredModules)) {
      $form_state->setErrorByName('module', $this->t('A module with name @module-name already exists.', [
        '@module-name' => $module_name_to_export,
      ]));
    }
    if (!$form_state->getValue('all') && !$this->getTypes($form_state)) {
      $form_state->setErrorByName('types', $this->t('You have to select at least one content entity type to be exported, or you need to declare you want to export everything.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = array_filter($form_state->getValues());
    $id = $values['id'] ?? Eme::getDefaultId();

    $export_plugin = $this->exportPluginManager->createInstance(
      $values['plugin'],
      [
        'types' => $form_state->getValue('all') ?: $this->getTypes($form_state),
        'module' => $values['module'] ?? Eme::getModuleName($id),
        'name' => $values['name'] ?? Eme::getModuleHumanName($id),
        'id-prefix' => $values['prefix'] ?? $id,
        'group' => $values['group'] ?? $id,
        'path' => NULL,
      ]);

    $this->batchRunner->setupBatch(
      $export_plugin,
      [get_class($this), 'finishBatch']
    );
  }

  /**
   * Used by machine name validate.
   */
  public static function machineNameExists($value, $element): bool {
    return FALSE;
  }

  /**
   * Returns the form structure to select entity types or bundles to export.
   *
   * @return array[]
   *   Form structure to select entity types or bundles to export.
   */
  protected function getTypesAndBundleElements(): array {
    $states = [
      'visible' => [
        ':input[name="all"]' => ['checked' => FALSE],
      ],
    ];
    $bundles = $this->entityBundleInfo->getAllBundleInfo();
    foreach (EmeCollectionUtils::getContentEntityTypes($this->entityTypeManager) as $entityTypeId => $label) {
      $elements[$entityTypeId] = [
        '#type' => 'container',
        '#states' => $states,
      ];
      $elements[$entityTypeId][$entityTypeId] = [
        '#type' => 'checkbox',
        '#title' => $label,
        '#return_value' => "{$entityTypeId}_has_bundles",
      ];
      if (!($entityType = $this->entityTypeManager->getDefinition($entityTypeId))->getKey('bundle')) {
        $elements[$entityTypeId][$entityTypeId]['#return_value'] = $entityTypeId;
        continue;
      }

      $typeStates = [
        'visible' => [
          ":input[name='types[$entityTypeId][$entityTypeId]']" => ['checked' => TRUE],
        ],
      ];
      $elements[$entityTypeId]["{$entityTypeId}_bundles"] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['form-type--boolean']],
        $entityTypeId => [
          '#type' => 'checkbox',
          '#title' => $this->t('All @entity-type types', ['@entity-type' => $entityType->getSingularLabel()]),
          '#return_value' => $entityTypeId,
          '#default_value' => $entityTypeId,
        ],
        '#states' => $typeStates,
        '#attached' => ['library' => ['eme/all-or-specific']],
      ];
      foreach ($bundles[$entityTypeId] as $bundleId => $data) {
        $elements[$entityTypeId]["{$entityTypeId}_bundles"]["$entityTypeId:$bundleId"] = [
          '#type' => 'checkbox',
          '#title' => $data['label'],
          '#return_value' => "$entityTypeId:$bundleId",
        ];
      }
      $elements[$entityTypeId]["{$entityTypeId}_hr"] = [
        '#type' => 'container',
        'hr' => isset($elements) ? ['#type' => 'html_tag', '#tag' => 'hr'] : [],
        '#states' => $typeStates,
      ];
    }
    return $elements;
  }

  /**
   * Returns the type identifier strings ready to be passed to an export config.
   *
   * This method also adds the corresponding errors to the form state when
   * needed.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return string[]
   *   The type identifier strings ready to be passed to an export config. These
   *   might be:
   *   - The entity type ID of a content entity: "node" or "path_alias".
   *   - Entity type ID and bundle, separated by a colon: "node:article",
   *     "taxonomy_term:tags".
   */
  protected function getTypes(FormStateInterface $formState): array {
    $typesBundles = [];
    foreach ($formState->getValue('types') as $type => $values) {
      switch ($values[$type]) {
        case $type:
          $typesBundles[$type] = $type;
          break;

        case "{$type}_has_bundles":
          $tmpValue = $values["{$type}_bundles"][$type] === $type
            ? [$type => $type]
            : array_filter($values["{$type}_bundles"]);
          // This has bundles, and the top-level checkbox was checked. So, if
          // the temporary value is empty, then the user didn't select neither
          // "all" not any other bundle.
          if (empty($tmpValue)) {
            $componentContainer = $formState->getCompleteForm()['row']['col_last']['types'][$type];
            $bundleOptions = [$this->t('All types')] + array_map(
              fn (array $data) => $data['label'],
              $this->entityBundleInfo->getBundleInfo($type),
            );
            $last = array_pop($bundleOptions);
            $formState->setErrorByName(
              "types][$type",
              $this->t('The %outermost checkbox is checked. Either select at least one of %inner-options or %inner-options-last underneath; or uncheck %outermost.', [
                '%outermost' => $componentContainer[$type]['#title'],
                '%inner-options' => implode(', ', $bundleOptions),
                '%inner-options-last' => $last,
              ]),
            );
          }
          $typesBundles += $tmpValue;
          break;
      }
    }
    return array_values($typesBundles);
  }

}
