<?php

declare(strict_types=1);

namespace Drupal\eme;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginInterface;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginManagerInterface;

/**
 * The reference discovery service.
 */
class DependencyDiscovery implements DependencyDiscoveryInterface {

  public function __construct(
    protected DiscoveryPluginManagerInterface $discoveryPluginManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getEntityDependencies(ContentEntityInterface $entity): array {
    return $this->getDependencies(EmeObject::createFromContentEntity($entity));
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityReverseDependencies(ContentEntityInterface $entity): array {
    return $this->getReverseDependencies(EmeObject::createFromContentEntity($entity));
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(EmeObject $emeObject): array {
    return array_reduce(
      $this->discoveryPluginManager->getDiscoveryPluginInstances(),
      function (array $carry, DiscoveryPluginInterface $plugin) use ($emeObject) {
        $carry = array_merge(
          $carry,
          $this->emeObjectsToKeyedArray($plugin->fetchReferences($emeObject)),
        );
        return $carry;
      },
      []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getReverseDependencies(EmeObject $emeObject): array {
    return array_reduce(
      $this->discoveryPluginManager->getDiscoveryPluginInstances(),
      function (array $carry, DiscoveryPluginInterface $plugin) use ($emeObject) {
        $carry = array_merge(
          $carry,
          $this->emeObjectsToKeyedArray($plugin->fetchReverseReferences($emeObject)),
        );
        return $carry;
      },
      []
    );
  }

  /**
   * Turns array of EME objects into an array keyed by the object IDs.
   *
   * @param \Drupal\eme\EmeObject[] $emeObjects
   *   The EME objects.
   *
   * @return \Drupal\eme\EmeObject[]
   *   The EME objects as keyed array.
   */
  protected function emeObjectsToKeyedArray(array $emeObjects): array {
    return array_reduce(
      $emeObjects,
      function (array $carry, EmeObject $emeObject): array {
        $carry[$emeObject->getId()] = $emeObject;
        return $carry;
      },
      [],
    );
  }

}
