<?php

declare(strict_types=1);

namespace Drupal\eme;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for reference discovery.
 */
interface DependencyDiscoveryInterface {

  /**
   * Discovers and returns the objects what the given entity depends on.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity which dependencies should be discovered.
   *
   * @return \Drupal\eme\EmeObject[]
   *   The dependencies of the entity.
   */
  public function getEntityDependencies(ContentEntityInterface $entity): array;

  /**
   * Discovers and returns the objects what the given entity depends on.
   *
   * @param \Drupal\eme\EmeObject $emeObject
   *   The entity which dependencies should be discovered.
   *
   * @return \Drupal\eme\EmeObject[][]
   *   The dependencies of the EME object.
   */
  public function getDependencies(EmeObject $emeObject): array;

  /**
   * Discovers and returns the objects which depend on the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity which dependencies should be discovered.
   *
   * @return \Drupal\eme\EmeObject[]
   *   The dependencies of the entity.
   */
  public function getEntityReverseDependencies(ContentEntityInterface $entity): array;

  /**
   * Discovers and returns the objects which depend on the object.
   *
   * @param \Drupal\eme\EmeObject $emeObject
   *   The entity which dependencies should be discovered.
   *
   * @return \Drupal\eme\EmeObject[]
   *   The dependencies of the EME object.
   */
  public function getReverseDependencies(EmeObject $emeObject): array;

}
