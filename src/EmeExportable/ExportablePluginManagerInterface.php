<?php

declare(strict_types=1);

namespace Drupal\eme\EmeExportable;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\EmeObjectRelation;

/**
 * Interface for object exporter plugin manager.
 */
interface ExportablePluginManagerInterface extends PluginManagerInterface {

  /**
   * Returns the plugin ID of the object exporter plugin based on (entity) type.
   *
   * @param string $type
   *   The object type (usually the type ID of the content entity).
   *
   * @return string
   *   The plugin ID of the object exporter plugin.
   */
  public function getPluginIdForType(string $type): string;

  /**
   * Creates a plugin instance for the given EmeObject.
   *
   * @param \Drupal\eme\EmeObject $object
   *   The EmeObject.
   *
   * @return \Drupal\eme\EmeExportable\ExportablePluginInterface|null
   *   The object exporter plugin instance, or null if it cannot be
   *   instantiated.
   */
  public function getInstanceForEmeObject(EmeObject $object): ?ExportablePluginInterface;

  /**
   * Creates a plugin instance for the given content entity instance.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity instance.
   *
   * @return \Drupal\eme\EmeExportable\ExportablePluginInterface|null
   *   The object exporter plugin instance, or null if it cannot be
   *   instantiated.
   */
  public function getInstanceForEntity(ContentEntityInterface $entity): ?ExportablePluginInterface;

  /**
   * Creates a plugin instance for the given EmeObjectRelation object.
   *
   * @param \Drupal\eme\EmeObjectRelation $relation
   *   The EmeObjectRelation object.
   *
   * @return \Drupal\eme\EmeExportable\ExportablePluginInterface|null
   *   The object exporter plugin instance, or null if it cannot be
   *   instantiated.
   */
  public function getInstanceFromRelation(EmeObjectRelation $relation): ?ExportablePluginInterface;

}
