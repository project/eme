<?php

declare(strict_types=1);

namespace Drupal\eme\EmeExportable;

use Drupal\eme\EmeObject;
use Drupal\eme\EmeObjectRelation;

/**
 * Interface for object exporter plugins.
 */
interface ExportablePluginInterface {

  /**
   * Sets the EmeObject.
   *
   * @param \Drupal\eme\EmeObject $object
   *   The EmeObject.
   */
  public function setObject(EmeObject $object): void;

  /**
   * Sets the EmeObjectRelation.
   *
   * @param \Drupal\eme\EmeObjectRelation $relation
   *   The EmeObjectRelation.
   */
  public function setRelation(EmeObjectRelation $relation): void;

  /**
   * Returns the EmeObjectRelation.
   *
   * @return \Drupal\eme\EmeObjectRelation
   *   The EmeObjectRelation.
   */
  public function getRelation(): EmeObjectRelation;

  /**
   * Returns migration process pipelines needed to import the exported object.
   *
   * @return array
   *   Migration process pipelines needed to import the exported object.
   */
  public function getProcessPipelines(): array;

  /**
   * Returns the values of the exported object.
   *
   * @return array
   *   Values of the exported object, keyed by the property name.
   */
  public function getValues(): array;

  /**
   * Returns the migration destination plugin configuration.
   *
   * @return array
   *   The migration destination plugin configuration of the exported object.
   */
  public function getDestination(): array;

  /**
   * Returns the type of the exported object.
   *
   * This is usually the entity type ID of the exported content entity.
   *
   * @return string
   *   Type of the exported object.
   */
  public function getEntityTypeId(): string;

  /**
   * Returns the bundle type of the exported object.
   *
   * This is usually the bundle ID of the exported content entity.
   *
   * @return string|null
   *   Bundle type of the exported object.
   */
  public function getBundle(): ?string;

  /**
   * Returns the list of the Drupal modules needed to migrate the object.
   *
   * This method must return all the Drupal modules which provide process or
   * destination plugins needed to execute the migration of the exported
   * object.
   *
   * @return string[]
   *   List of the Drupal modules needed to migrate the object.
   */
  public function getModuleDependencies(): array;

  /**
   * Returns the object identifier of the exported object.
   *
   * Usually, this is the entity ID of a content entity.
   *
   * @return string
   *   The object identifier of the exported object.
   */
  public function getObjectId(): string;

  /**
   * Returns the list of a single object instance's identifiers.
   *
   * @return array[]
   *   The list of a single object instance's identifiers. Keys are the object
   *   properties, value is an array with the type of the identifier property
   *   (e.g. ['type' => 'string']).
   */
  public function getObjectIdentifierKeysAndTypes(): array;

  /**
   * Classifies whether the discovered dependency is required or optional.
   *
   * @param string $dependency_type
   *   The type of the exported object, usually the entity type ID of the
   *   content entity.
   * @param string|null $dependency_bundle
   *   The bundle of the dependency object, usually the bundle type ID of the
   *   content entity.
   *
   * @return string
   *   'required' or 'optional'.
   */
  public function dependencyIsRequiredForMigration(string $dependency_type, ?string $dependency_bundle): bool;

  /**
   * Whether the current object should be excluded or not.
   *
   * @return bool
   *   TRUE if the current object should be excluded, FALSE otherwise.
   */
  public function shouldBeExcluded(): bool;

}
