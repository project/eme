<?php

namespace Drupal\eme\EmeExportable\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * EME object parser plugin annotation.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class EmeExportable extends Plugin {

  /**
   * Type of the object the plugin applies on.
   *
   * @var string
   */
  public $type;

}
