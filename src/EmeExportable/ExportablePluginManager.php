<?php

declare(strict_types=1);

namespace Drupal\eme\EmeExportable;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\eme\EmeExportable\Annotation\EmeExportable;
use Drupal\eme\EmeFieldValueExporter\FieldValueExporterManagerInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\EmeObjectRelation;
use Psr\Log\LoggerInterface;

/**
 * Plugin manager of object exporter plugins.
 */
class ExportablePluginManager extends DefaultPluginManager implements ExportablePluginManagerInterface {

  /**
   * The plugin group identifier.
   *
   * Used as cache ID and also in the plugin alter hook.
   *
   * @const string
   */
  const PLUGIN_GROUP_ID = 'eme_exportable_plugins';

  /**
   * ID of the fallback plugin.
   *
   * @const string
   */
  const FALLBACK_PLUGIN_ID = 'entity';

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The field value exporter plugin manager.
   *
   * @var \Drupal\eme\EmeFieldValueExporter\FieldValueExporterManagerInterface
   */
  protected $fieldValueExporterManager;

  /**
   * Constructs an ObjectExporterManager instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\eme\EmeFieldValueExporter\FieldValueExporterManagerInterface $field_value_exporter_manager
   *   The field value exporter manager.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend, LoggerInterface $logger, FieldValueExporterManagerInterface $field_value_exporter_manager) {
    parent::__construct(
      'Plugin/Eme/Exportable',
      $namespaces,
      $module_handler,
      ExportablePluginInterface::class,
      EmeExportable::class
    );

    $this->alterInfo(static::PLUGIN_GROUP_ID);
    $this->setCacheBackend($cache_backend, static::PLUGIN_GROUP_ID);
    $this->logger = $logger;
    $this->fieldValueExporterManager = $field_value_exporter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceForEmeObject(EmeObject $object): ?ExportablePluginInterface {
    $plugin_id = $this->getPluginIdForType($object->getPluginId());
    $plugin = $this->createExportablePluginInstance($plugin_id);
    $plugin->setObject($object);
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginIdForType(string $type): string {
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if (($definition['type'] ?? NULL) !== $type) {
        continue;
      }
      return $plugin_id;
    }
    return static::FALLBACK_PLUGIN_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceFromRelation(EmeObjectRelation $relation): ?ExportablePluginInterface {
    $exportable_plugin_id = explode(EmeObjectRelation::RELATION_ID_SEPARATOR, $relation->getId())[0];
    $plugin = $this->createExportablePluginInstance($exportable_plugin_id);
    $plugin->setRelation($relation);
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceForEntity(ContentEntityInterface $entity): ?ExportablePluginInterface {
    $plugin_id = $this->getPluginIdForType($entity->getEntityTypeId());
    $plugin = $this->createExportablePluginInstance($plugin_id);
    $plugin->setObject(EmeObject::createFromContentEntity($entity));
    return $plugin;
  }

  /**
   * Creates an object exporter plugin instance.
   *
   * @return \Drupal\eme\EmeExportable\ExportablePluginInterface|null
   *   The object exporter plugin instance, or null if it cannot be
   *   instantiated.
   */
  protected function createExportablePluginInstance(): ?ExportablePluginInterface {
    $instance = $this->createInstance(...func_get_args());
    if ($instance !== NULL) {
      assert($instance instanceof ExportablePluginInterface);
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $plugin_definition = $this->getDefinition($plugin_id);
    $plugin_class = DefaultFactory::getPluginClass($plugin_id, $plugin_definition);
    // If the plugin provides a factory method, pass the container to it.
    if (is_subclass_of($plugin_class, ContainerFactoryPluginInterface::class)) {
      // @codingStandardsIgnoreLine
      $plugin = $plugin_class::create(\Drupal::getContainer(), $configuration, $plugin_id, $plugin_definition, $this->fieldValueExporterManager); // @phpstan-ignore-line
    }
    else {
      $plugin = new $plugin_class($configuration, $plugin_id, $plugin_definition, $this->fieldValueExporterManager);
    }
    return $plugin;
  }

}
