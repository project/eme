<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\FieldValueExporter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\pathauto\PathautoState;

/**
 * Field value exporter for path fields if Pathauto module is present.
 *
 * @EmeFieldValueExporter(
 *   id = "path",
 *   field_types = {"path"},
 *   provider = "pathauto"
 * )
 */
class PathField extends General {

  /**
   * {@inheritdoc}
   */
  public function getProcessPipeline(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDependencies(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldValue(FieldItemListInterface $item_list): mixed {
    // @phpstan-ignore-next-line (Pathauto is not marked as test dependency.)
    return [['pathauto' => PathautoState::SKIP]];
  }

}
