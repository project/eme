<?php

namespace Drupal\eme\Plugin\Eme\FieldValueExporter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\eme\EmeFieldValueExporter\FieldValueExporterPluginInterface;

/**
 * Field value exporter for layout section fields.
 *
 * @EmeFieldValueExporter(
 *   id = "layout_section",
 *   field_types = {"layout_section"},
 *   provider = "layout_builder"
 * )
 */
class LayoutSection implements FieldValueExporterPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getProcessPipeline(FieldDefinitionInterface $field_definition) {
    // Layout builder expects an object field value.
    return [
      'plugin' => 'sub_process',
      'source' => $field_definition->getName(),
      'process' => [
        'section' => [
          'plugin' => 'callback',
          'callable' => 'unserialize',
          'source' => 'section',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldValue(FieldItemListInterface $item_list): mixed {
    if ($item_list->isEmpty()) {
      return NULL;
    }

    $field_value = $item_list->getValue();
    foreach ($field_value as $delta => $field_item_value) {
      $field_value[$delta]['section'] = serialize($field_value[$delta]['section']);
    }

    return $field_value;
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDependencies(): array {
    return [];
  }

}
