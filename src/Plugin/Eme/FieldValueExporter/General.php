<?php

namespace Drupal\eme\Plugin\Eme\FieldValueExporter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\Type\BooleanInterface;
use Drupal\Core\TypedData\Type\FloatInterface;
use Drupal\Core\TypedData\Type\IntegerInterface;
use Drupal\eme\EmeFieldValueExporter\FieldValueExporterPluginInterface;

/**
 * Fallback field value exporter.
 *
 * Works well with field values with common functionality.
 *
 * @EmeFieldValueExporter(
 *   id = "general"
 * )
 */
class General implements FieldValueExporterPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getProcessPipeline(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldValue(FieldItemListInterface $item_list): mixed {
    if ($item_list->isEmpty()) {
      // Some fields do not like missing values, e.g.
      // entity_reference_revisions.
      return NULL;
    }

    $storage_definition = $item_list->getFieldDefinition()->getFieldStorageDefinition();
    $property_definitions = array_filter(
      $storage_definition->getPropertyDefinitions(),
      function (DataDefinitionInterface $def) {
        return !$def->isComputed();
      }
    );
    $property_count = count($property_definitions);
    $main_property_name = $storage_definition->getMainPropertyName();

    $complex_prop = $property_count > 1 || count($item_list) > 1 || !$main_property_name;
    $field_value = $complex_prop ? $item_list->getValue() : $item_list->{$main_property_name};

    // In some cases, core expects that field property values follow their
    // data type (e.g. taxonomy_build_node_index() expects that the target
    // term ID is integer).
    // Sadly the value getters don't do that.
    if ($complex_prop) {
      foreach ($field_value as $delta => $delta_value) {
        foreach ($delta_value as $property => $prop_value) {
          $field_value[$delta][$property] = $this->castValue($property, $property_definitions, $prop_value);
        }
      }
    }
    // This is a simple field: there is only one field item, with one property,
    // with a single value.
    else {
      $field_value = $this->castValue($main_property_name, $property_definitions, $field_value);
    }

    return $field_value;
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDependencies(): array {
    return [];
  }

  /**
   * Casts the given value to the correct type.
   *
   * @param string $property
   *   The name of the property.
   * @param \Drupal\Core\TypedData\DataDefinitionInterface[] $property_definitions
   *   The property definitions of the field item.
   * @param mixed $value
   *   The raw value to cast.
   *
   * @return mixed
   *   Value with the correct type.
   */
  protected function castValue(string $property, array $property_definitions, mixed $value): mixed {
    if ($this->propertyShouldBeCastedToInt($property, $property_definitions)) {
      $value = (int) $value;
    }
    elseif ($this->propertyShouldBeCastedToFloat($property, $property_definitions)) {
      $value = (float) $value;
    }
    elseif ($this->propertyShouldBeCastedToBool($property, $property_definitions)) {
      $value = (bool) $value;
    }
    return $value;
  }

  /**
   * Determines whether the property should be casted to integer.
   *
   * @param string $property
   *   The name of the property.
   * @param \Drupal\Core\TypedData\DataDefinitionInterface[] $property_definitions
   *   The property definitions of the field item.
   *
   * @return bool
   *   Whether the property should be casted to integer.
   */
  protected function propertyShouldBeCastedToInt(string $property, array $property_definitions): bool {
    return in_array(
      IntegerInterface::class,
      class_implements($property_definitions[$property]->getClass())
    );
  }

  /**
   * Determines whether the property should be casted to boolean.
   *
   * @param string $property
   *   The name of the property.
   * @param \Drupal\Core\TypedData\DataDefinitionInterface[] $property_definitions
   *   The property definitions of the field item.
   *
   * @return bool
   *   Whether the property should be casted to boolean.
   */
  protected function propertyShouldBeCastedToBool(string $property, array $property_definitions): bool {
    return in_array(
      BooleanInterface::class,
      class_implements($property_definitions[$property]->getClass())
    );
  }

  /**
   * Determines whether the property should be casted to double.
   *
   * @param string $property
   *   The name of the property.
   * @param \Drupal\Core\TypedData\DataDefinitionInterface[] $property_definitions
   *   The property definitions of the field item.
   *
   * @return bool
   *   Whether the property should be casted to double.
   */
  protected function propertyShouldBeCastedToFloat(string $property, array $property_definitions): bool {
    return in_array(
      FloatInterface::class,
      class_implements($property_definitions[$property]->getClass())
    );
  }

}
