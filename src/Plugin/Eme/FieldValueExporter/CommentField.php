<?php

namespace Drupal\eme\Plugin\Eme\FieldValueExporter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\Type\IntegerInterface;
use Drupal\eme\EmeFieldValueExporter\FieldValueExporterPluginInterface;

/**
 * Field value exporter for comment fields.
 *
 * @EmeFieldValueExporter(
 *   id = "comment_field",
 *   field_types = {"comment"},
 *   provider = "comment"
 * )
 */
class CommentField implements FieldValueExporterPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getProcessPipeline(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldValue(FieldItemListInterface $item_list): mixed {
    if ($item_list->isEmpty()) {
      // Some fields do not like missing values, e.g.
      // entity_reference_revisions.
      return NULL;
    }

    $field_value = $item_list->getValue();
    $property_definitions = $item_list->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getPropertyDefinitions();

    foreach ($field_value as $delta => $item) {
      foreach ($item as $property_key => $property_value) {
        // Comment statistics service always overrides last comment timestamp.
        if ($property_key === 'last_comment_timestamp') {
          unset($field_value[$delta][$property_key]);
          continue;
        }
        if (
          in_array(
            IntegerInterface::class,
            class_implements($property_definitions[$property_key]->getClass())
          )
        ) {
          $field_value[$delta][$property_key] = (int) $property_value;
        }
      }
    }
    return $field_value ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDependencies(): array {
    return [];
  }

}
