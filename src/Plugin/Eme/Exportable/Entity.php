<?php

namespace Drupal\eme\Plugin\Eme\Exportable;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\eme\EmeExportable\ExportablePluginInterface;
use Drupal\eme\EmeFieldValueExporter\FieldValueExporterManagerInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\EmeObjectRelation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default object exporter plugin of content entities.
 *
 * @EmeExportable(
 *   id = "entity",
 *   type = "",
 * )
 */
class Entity extends PluginBase implements ExportablePluginInterface, ContainerFactoryPluginInterface {

  /**
   * The EME field value exporter manager.
   *
   * @var \Drupal\eme\EmeFieldValueExporter\FieldValueExporterManagerInterface
   */
  protected $fieldValueExporterManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The exported content entity instance.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * The storage handler of the exported entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $entityStorage;

  /**
   * The entity type ID.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The bundle.
   *
   * @var string|null
   */
  protected $bundle;

  /**
   * Constructs a new Entity instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\eme\EmeFieldValueExporter\FieldValueExporterManagerInterface|null $field_value_exporter_manager
   *   The field value exporter manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ?FieldValueExporterManagerInterface $field_value_exporter_manager, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fieldValueExporterManager = $field_value_exporter_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeId = $plugin_definition['type'] ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, FieldValueExporterManagerInterface $field_value_exporter_manager = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $field_value_exporter_manager,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setObject(EmeObject $object): void {
    $this->entityTypeId = $this->entityTypeId ?? $object->getPluginId();
    $this->entityStorage = $this->entityTypeManager->getStorage($this->getEntityTypeId());
    $ids = $object->getObjectIds();
    $revision_id = count($ids) > 1 && !empty($ids[1]) ? $ids[1] : NULL;
    $this->entity = $revision_id
      // @phpstan-ignore-next-line (see https://drupal.org//i/3383215)
      ? $this->entityStorage->loadRevision($revision_id)
      : $this->entityStorage->load($ids[0]);
    $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
    $bundle_key = $definition->getKey('bundle') && $definition->getBundleEntityType();
    $this->bundle = $bundle_key ? $this->entity->bundle() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setRelation(EmeObjectRelation $relation): void {
    $pieces = explode(EmeObjectRelation::RELATION_ID_SEPARATOR, $relation->getId());
    $this->entityTypeId = $this->entityTypeId ?? $pieces[1];
    $this->entityStorage = $this->entityTypeManager->getStorage($this->getEntityTypeId());
    $this->bundle = $pieces[2] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestination(bool $include_translations = TRUE): array {
    $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
    $destination_plugin_base = $definition->getKey('revision')
      ? 'entity_complete'
      : 'entity';
    $destination_plugin = implode(PluginBase::DERIVATIVE_SEPARATOR, [
      $destination_plugin_base,
      $this->getEntityTypeId(),
    ]);
    return [
      'plugin' => $destination_plugin,
      'translations' => $include_translations && !empty($definition->getKey('langcode')),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRelation(): EmeObjectRelation {
    $ids = [$this->getEntityTypeId()];
    if (!is_null($this->getBundle())) {
      $ids[] = $this->getBundle();
    }
    return new EmeObjectRelation($this->getPluginId(), ...$ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    return $this->entityTypeId;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundle(): ?string {
    return $this->bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessPipelines(): array {
    return array_map(
      function (FieldDefinitionInterface $field_definition) {
        $plugin = $this->fieldValueExporterManager->getInstanceForFieldDefinition($field_definition);
        return $plugin->getProcessPipeline($field_definition);
      },
      $this->getExportedFieldDefinitions()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(): array {
    $entity_values_all = [
      $this->getEntityValues($this->entity),
    ];

    // Add translations.
    foreach ($this->entity->getTranslationLanguages(FALSE) as $language) {
      $translation = $this->entity->getTranslation($language->getId());
      $entity_values_all[] = $this->getEntityValues($translation);
    }
    return $entity_values_all;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityValues(ContentEntityInterface $entity) {
    $values = [];
    foreach ($this->getExportedFieldDefinitions() as $field_name => $field_definition) {
      $values[$field_name] = $this->fieldValueExporterManager
        ->getInstanceForFieldDefinition($field_definition)
        ->getFieldValue($entity->get($field_name));
    }
    return $values;
  }

  /**
   * Returns the list of the exported field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   List of the exported field definitions.
   */
  protected function getExportedFieldDefinitions(): array {
    $all_fields = $this->entityFieldManager->getFieldDefinitions($this->getEntityTypeId(), $this->getBundle() ?? $this->getEntityTypeId());
    $computed_fields = array_filter(
      $all_fields,
      function (FieldDefinitionInterface $def) {
        return $def->isComputed();
      }
    );
    // Path fields need special treatment if Pathauto Patterns is installed -
    // which is a common standard.
    foreach (array_keys($computed_fields) as $name) {
      if ($computed_fields[$name]->getType() === 'path') {
        unset($computed_fields[$name]);
      }
    }
    unset($computed_fields['moderation_state']);
    return array_diff_key($all_fields, $computed_fields);
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDependencies(): array {
    $dependencies = [];
    foreach ($this->getExportedFieldDefinitions() as $field_definition) {
      $dependencies = array_merge(
        $dependencies,
        $this->fieldValueExporterManager
          ->getInstanceForFieldDefinition($field_definition)
          ->getModuleDependencies()
      );
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectId(): string {
    return (string) $this->entity->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectIdentifierKeysAndTypes(): array {
    $entity_definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
    $field_definitions = $this->getExportedFieldDefinitions();
    $identifiers = [];
    foreach (['id', 'revision', 'langcode'] as $key_name) {
      if ($key = $entity_definition->getKey($key_name)) {
        $key_type = $field_definitions[$key]->getType() === 'integer'
          ? 'integer'
          : 'string';
        $identifiers[$key] = [
          'type' => $key_type,
        ];
      }
    }
    return $identifiers;
  }

  /**
   * {@inheritdoc}
   */
  public function dependencyIsRequiredForMigration(string $dependency_type, ?string $dependency_bundle): bool {
    return $dependency_type === 'pathauto_state';
  }

  /**
   * {@inheritdoc}
   */
  public function shouldBeExcluded(): bool {
    return FALSE;
  }

}
