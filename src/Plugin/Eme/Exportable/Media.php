<?php

namespace Drupal\eme\Plugin\Eme\Exportable;

/**
 * Exporter plugin of media entities.
 *
 * @EmeExportable(
 *   id = "media",
 *   type = "media",
 *   provider = "media"
 * )
 */
class Media extends Entity {

  /**
   * {@inheritdoc}
   */
  protected function getExportedFieldDefinitions(): array {
    $field_defs = parent::getExportedFieldDefinitions();
    // Media always sets a new revision_created date.
    unset($field_defs['revision_created']);
    return $field_defs;
  }

}
