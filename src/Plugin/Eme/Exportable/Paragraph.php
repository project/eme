<?php

namespace Drupal\eme\Plugin\Eme\Exportable;

/**
 * Export plugin of paragraph entities.
 *
 * @EmeExportable(
 *   id = "paragraph",
 *   type = "paragraph",
 *   provider = "paragraphs"
 * )
 */
class Paragraph extends Entity {

  /**
   * {@inheritdoc}
   */
  public function getDestination(bool $include_translations = TRUE): array {
    return [
      'plugin' => 'entity_reference_revisions:paragraph',
      'translations' => $include_translations,
    ];
  }

}
