<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\Exportable;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\eme\EmeExportable\ExportablePluginInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\EmeObjectRelation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Export plugin of poll votes belonging to a specific poll choice.
 *
 * @EmeExportable(
 *   id = "comment_entity_statistics",
 *   type = "comment_entity_statistics",
 *   provider = "comment"
 * )
 */
class CommentEntityStatistics extends PluginBase implements ExportablePluginInterface, ContainerFactoryPluginInterface {

  const DB_TABLE = 'comment_entity_statistics';

  /**
   * The database where comment entity statistics are stored.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The ID of the entity the comment statistics item belongs to.
   *
   * @var string|int
   */
  protected $entityId;

  /**
   * The type ID of the entity the comment statistics item belongs to.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * Name of the comment field.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Constructs a new CommentEntityStatistics export plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Connection $database,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setObject(EmeObject $object): void {
    [
      $this->entityId,
      $this->entityTypeId,
      $this->fieldName,
    ] = $object->getObjectIds();
  }

  /**
   * {@inheritdoc}
   */
  public function setRelation(EmeObjectRelation $relation): void {
  }

  /**
   * {@inheritdoc}
   */
  public function getRelation(): EmeObjectRelation {
    return new EmeObjectRelation($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessPipelines(): array {
    // There is no standardized way to get column names in tables. But since we
    // are here we can assume that there is at least one record.
    $record = $this->database->select(static::DB_TABLE, 'dt')
      ->fields('dt')
      ->range(0, 1)
      ->execute()
      ->fetch(\PDO::FETCH_ASSOC);
    return array_combine(array_keys($record), array_keys($record));
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(): array {
    return $this->database->select(static::DB_TABLE, 'dt')
      ->fields('dt')
      ->condition('dt.entity_id', $this->entityId)
      ->condition('dt.entity_type', $this->entityTypeId)
      ->condition('dt.field_name', $this->fieldName)
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * {@inheritdoc}
   */
  public function getDestination(): array {
    $ids = $this->getObjectIdentifierKeysAndTypes();
    $non_id_columns = array_keys(
      array_diff_key(
        $this->getProcessPipelines(),
        $ids
      )
    );
    return [
      'plugin' => 'table',
      'table_name' => static::DB_TABLE,
      'id_fields' => $ids,
      'fields' => array_combine($non_id_columns, $non_id_columns),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    return 'comment_entity_statistics';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundle(): ?string {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDependencies(): array {
    return ['migrate_plus:migrate_plus'];
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectId(): string {
    return implode('-', [$this->entityId, $this->entityTypeId, $this->fieldName]);
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectIdentifierKeysAndTypes(): array {
    return [
      'entity_id' => ['type' => 'integer'],
      'entity_type' => ['type' => 'string'],
      'field_name' => ['type' => 'string'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function dependencyIsRequiredForMigration(string $dependency_type, ?string $dependency_bundle): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldBeExcluded(): bool {
    return FALSE;
  }

}
