<?php

namespace Drupal\eme\Plugin\Eme\Exportable;

/**
 * Exporter plugin of comment entities.
 *
 * @EmeExportable(
 *   id = "comment",
 *   type = "comment",
 *   provider = "comment"
 * )
 */
class Comment extends Entity {

  /**
   * {@inheritdoc}
   */
  public function dependencyIsRequiredForMigration(string $dependency_type, ?string $dependency_bundle): bool {
    return $dependency_type === 'user';
  }

}
