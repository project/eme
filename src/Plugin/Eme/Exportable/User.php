<?php

namespace Drupal\eme\Plugin\Eme\Exportable;

/**
 * Controls exporting of user accounts.
 *
 * @EmeExportable(
 *   id = "user",
 *   type = "user",
 *   provider = "user"
 * )
 */
class User extends Entity {

  /**
   * {@inheritdoc}
   */
  public function shouldBeExcluded(): bool {
    // Anonymous user shouldn't be exported.
    return empty($this->getObjectId());
  }

  /**
   * {@inheritdoc}
   */
  public function dependencyIsRequiredForMigration(string $dependency_type, ?string $dependency_bundle): bool {
    return parent::dependencyIsRequiredForMigration($dependency_type, $dependency_bundle) ||
      // The migration of users without missing user picture throws notice.
      $dependency_type === 'file';
  }

}
