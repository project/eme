<?php

declare(strict_types=1);

// cspell:ignore webform
namespace Drupal\eme\Plugin\Eme\Exportable;

use Drupal\path_alias\PathAliasInterface;

/**
 * Export plugin of path alias entities - excludes Webform aliases.
 *
 * @EmeExportable(
 *   id = "path_alias",
 *   type = "path_alias",
 *   provider = "webform",
 * )
 */
class PathAlias extends Entity {

  /**
   * {@inheritdoc}
   */
  public function shouldBeExcluded(): bool {
    assert($this->entity instanceof PathAliasInterface);
    return str_starts_with($this->entity->getPath(), '/webform/contact');
  }

}
