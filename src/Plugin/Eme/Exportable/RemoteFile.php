<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\Exportable;

/**
 * Exportable plugin of remote files.
 *
 * These entities aren't stored, they are using null storage. All of them must
 * be excluded.
 *
 * @EmeExportable(
 *   id = "remote_file",
 *   type = "remote_file",
 *   provider = "file_url",
 * )
 */
class RemoteFile extends Entity {

  /**
   * {@inheritdoc}
   */
  public function shouldBeExcluded(): bool {
    return TRUE;
  }

}
