<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\Exportable;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\eme\EmeExportable\ExportablePluginInterface;
use Drupal\eme\EmeFieldValueExporter\FieldValueExporterManagerInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\EmeObjectRelation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Export plugin for pathauto state of entity canonical aliases.
 *
 * @EmeExportable(
 *   id = "pathauto_state",
 *   type = "pathauto_state",
 *   provider = "pathauto"
 * )
 */
class PathAutoState extends PluginBase implements ExportablePluginInterface, ContainerFactoryPluginInterface {

  const DB_TABLE = 'key_value';

  /**
   * The key-value storage factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyvalue;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The ID of the entity the pathauto state belongs to.
   *
   * @var string|int
   */
  protected $entityId;

  /**
   * The type ID of the entity the pathauto state belongs to.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PollVote export plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyvalue
   *   The key-value storage factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, KeyValueFactoryInterface $keyvalue, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->keyvalue = $keyvalue;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?FieldValueExporterManagerInterface $field_value_exporter_manager = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('keyvalue'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setObject(EmeObject $object): void {
    [
      $this->entityTypeId,
      $this->entityId,
    ] = $object->getObjectIds();
  }

  /**
   * {@inheritdoc}
   */
  public function setRelation(EmeObjectRelation $relation): void {
    $pieces = explode(EmeObjectRelation::RELATION_ID_SEPARATOR, $relation->getId());
    $this->entityTypeId = $pieces[1] ?? NULL;
    $this->entityId = $pieces[2] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelation(): EmeObjectRelation {
    return new EmeObjectRelation($this->getPluginId(), $this->entityTypeId);
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessPipelines(): array {
    // There is no standardized way to get column names in tables. But since we
    // are here we can assume that there is at least one record.
    $record = $this->database->select(static::DB_TABLE, 'dt')
      ->fields('dt')
      ->range(0, 1)
      ->execute()
      ->fetch(\PDO::FETCH_ASSOC);
    return array_combine(array_keys($record), array_keys($record));
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(): array {
    return $this->database->select(static::DB_TABLE, 'dt')
      ->fields('dt')
      ->condition('dt.collection', "pathauto_state.$this->entityTypeId")
      ->condition('dt.name', (string) $this->entityId)
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * {@inheritdoc}
   */
  public function getDestination(): array {
    $ids = $this->getObjectIdentifierKeysAndTypes();
    $non_id_columns = array_keys(
      array_diff_key(
        $this->getProcessPipelines(),
        $ids
      )
    );
    return [
      'plugin' => 'table',
      'table_name' => static::DB_TABLE,
      'id_fields' => $ids,
      'fields' => array_combine($non_id_columns, $non_id_columns),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDirectDependencies(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getReverseDependencies(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    return 'pathauto_state';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundle(): ?string {
    return $this->entityTypeId;
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDependencies(): array {
    return ['migrate_plus:migrate_plus'];
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectId(): string {
    return implode('-', [$this->entityTypeId, $this->entityId]);
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectIdentifierKeysAndTypes(): array {
    return [
      'collection' => ['type' => 'string'],
      'name' => ['type' => 'string'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function dependencyIsRequiredForMigration(string $dependency_type, ?string $dependency_bundle): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldBeExcluded(): bool {
    return FALSE;
  }

}
