<?php

namespace Drupal\eme\Plugin\Eme\Exportable;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\eme\EmeExportable\ExportablePluginInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\EmeObjectRelation;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Export plugin of poll votes belonging to a specific poll choice.
 *
 * @EmeExportable(
 *   id = "poll_vote",
 *   type = "poll_vote",
 *   provider = "poll"
 * )
 */
class PollVote extends PluginBase implements ExportablePluginInterface, ContainerFactoryPluginInterface {

  /**
   * Table of the poll votes.
   *
   * @const string
   */
  const DB_TABLE = 'poll_vote';

  /**
   * The database where poll votes are stored.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Identifier of the poll choice entity the exported poll votes belong to.
   *
   * @var string
   */
  protected $pollChoiceId;

  /**
   * Constructs a new PollVote export plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, DiscoveryPluginManagerInterface $reference_discovery_manager = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setObject(EmeObject $object): void {
    $this->pollChoiceId = $object->getObjectIds()[0];
  }

  /**
   * {@inheritdoc}
   */
  public function setRelation(EmeObjectRelation $relation): void {
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessPipelines(): array {
    // There is no standardized way to get column names in tables. But since we
    // are here we can assume that there is at least one record.
    $record = $this->database->select(static::DB_TABLE, 'p')
      ->fields('p')
      ->range(0, 1)
      ->execute()
      ->fetch(\PDO::FETCH_ASSOC);
    return array_combine(array_keys($record), array_keys($record));
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(): array {
    return $this->getPollVoteRecordsForChoiceId($this->pollChoiceId);
  }

  /**
   * {@inheritdoc}
   */
  public function getDestination(): array {
    $ids = $this->getObjectIdentifierKeysAndTypes();
    $non_id_columns = array_diff(
      $this->getProcessPipelines(),
      array_keys($ids)
    );
    return [
      'plugin' => 'table',
      'table_name' => static::DB_TABLE,
      'id_fields' => $ids,
      'fields' => $non_id_columns,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRelation(): EmeObjectRelation {
    return new EmeObjectRelation($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    return 'poll_vote';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundle(): ?string {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getPollVoteRecordsForChoiceId(string $poll_choice_id): array {
    return $this->database->select(static::DB_TABLE, 'p')
      ->fields('p')
      ->condition('p.chid', $poll_choice_id)
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDependencies(): array {
    return ['migrate_plus:migrate_plus'];
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectId(): string {
    return $this->pollChoiceId;
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectIdentifierKeysAndTypes(): array {
    return [
      'chid' => ['type' => 'integer'],
      'pid' => ['type' => 'integer'],
      'uid' => ['type' => 'integer'],
      'hostname' => ['type' => 'string'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function dependencyIsRequiredForMigration(string $dependency_type, ?string $dependency_bundle): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldBeExcluded(): bool {
    return FALSE;
  }

}
