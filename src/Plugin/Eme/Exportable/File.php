<?php

namespace Drupal\eme\Plugin\Eme\Exportable;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Exporter for file entities.
 *
 * @EmeExportable(
 *   id = "file",
 *   type = "file",
 *   provider = "file"
 * )
 */
class File extends Entity {

  /**
   * {@inheritdoc}
   */
  public function getProcessPipelines(): array {
    $process_pipelines = array_map(
      function (FieldDefinitionInterface $field_definition) {
        $plugin = $this->fieldValueExporterManager->getInstanceForFieldDefinition($field_definition);
        return $plugin->getProcessPipeline($field_definition);
      },
      $this->getExportedFieldDefinitions()
    );

    $this->mergeUriProcessPipelines($process_pipelines);
    return $process_pipelines;
  }

  /**
   * Merges extra process pipelines to migration which copy the source asset.
   *
   * @param array $process_pipelines
   *   The preexisting process pipelines.
   */
  protected function mergeUriProcessPipelines(array &$process_pipelines): void {
    if (!isset($process_pipelines['uri'])) {
      return;
    }

    // File asset migration requires a complex process.
    $preceding_processes = [
      'source_file_scheme' => [
        [
          'plugin' => 'explode',
          'delimiter' => '://',
          'source' => 'uri',
        ],
        [
          'plugin' => 'extract',
          'index' => [0],
        ],
        [
          'plugin' => 'skip_on_empty',
          'method' => 'row',
        ],
      ],
      'source_file_path' => [
        [
          'plugin' => 'explode',
          'delimiter' => '://',
          'source' => 'uri',
        ],
        [
          'plugin' => 'extract',
          'index' => [1],
        ],
        [
          'plugin' => 'skip_on_empty',
          'method' => 'row',
        ],
      ],
      'source_full_path' => [
        [
          'plugin' => 'concat',
          // DIRECTORY_SEPARATOR?
          'delimiter' => '/',
          'source' => [
            'constants/eme_file_path',
            '@source_file_scheme',
            '@source_file_path',
          ],
        ],
      ],
    ];

    $process_pipelines['uri'] = [
      'plugin' => 'file_copy',
      'source' => [
        '@source_full_path',
        'uri',
      ],
    ];

    $process_pipelines = array_merge(
      $preceding_processes,
      $process_pipelines
    );
  }

  /**
   * {@inheritdoc}
   */
  public function dependencyIsRequiredForMigration(string $dependency_type, ?string $dependency_bundle): bool {
    // Crop entities should be migrated before files.
    return $dependency_type === 'crop';
  }

}
