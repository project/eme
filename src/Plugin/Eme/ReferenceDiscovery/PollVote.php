<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Poll vote dependency discovery plugin.
 *
 * @ReferenceDiscovery(
 *   id = "poll_vote",
 *   type = "poll_vote",
 *   provider = "poll"
 * )
 */
class PollVote extends DiscoveryPluginBase {

  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager,
    protected Connection $database,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $entityFieldManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fetchReferences(EmeObject $emeObject): array {
    if ($emeObject->getPluginId() !== 'poll_vote') {
      return [];
    }

    $userIds = array_reduce(
      $this->getPollVoteRecordsQueryForChoiceId(current($emeObject->getObjectIds()))
        ->execute()
        ->fetchAll(),
      function (array $carry, \stdClass $record): array {
        $carry = array_unique(array_merge(
          $carry,
          [$record->uid]
        ));
        return $carry;
      },
      []
    );
    return array_map(
      fn (string $uid): EmeObject => new EmeObject('user', [$uid]),
      $userIds
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fetchReverseReferences(EmeObject $emeObject): array {
    if ($emeObject->getPluginId() !== 'poll_choice') {
      return [];
    }
    $choiceId = current($emeObject->getObjectIds());
    $voteCount = (int) $this->getPollVoteRecordsQueryForChoiceId($choiceId)
      ->countQuery()
      ->execute()
      ->fetchField();
    return $voteCount ? [new EmeObject('poll_vote', [$choiceId])] : [];
  }

  /**
   * Returns all the poll voted of the given choice.
   *
   * @param string $pollChoiceId
   *   The ID of the poll choice.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   The query.
   */
  protected function getPollVoteRecordsQueryForChoiceId(string $pollChoiceId): SelectInterface {
    return $this->database->select('poll_vote', 'p')
      ->fields('p')
      ->condition('p.chid', $pollChoiceId);
  }

}
