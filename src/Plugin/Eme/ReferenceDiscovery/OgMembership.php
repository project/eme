<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;

/**
 * Organic group membership discovery plugin.
 *
 * @ReferenceDiscovery(
 *   id = "og_membership",
 *   provider = "og",
 * )
 */
class OgMembership extends DiscoveryPluginBase {

  /**
   * {@inheritdoc}
   */
  public function fetchReverseReferences(EmeObject $emeObject): array {
    $storage = $this->entityTypeManager->getStorage('og_membership');
    $ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('entity_type', $emeObject->getPluginId())
      ->condition('entity_id', current($emeObject->getObjectIds()))
      ->execute();

    return array_map(
      fn (string $id): EmeObject => new EmeObject('og_membership', [$id]),
      $ids,
    );
  }

}
