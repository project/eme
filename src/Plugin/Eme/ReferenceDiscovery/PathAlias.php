<?php

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;

/**
 * Path alias discovery plugin.
 *
 * Logically, path aliases should be 'reverse' references: when their
 * destination entity is missing, there is no need for them (imho).
 *
 * But if Pathauto is installed, and the related entity has pattern, then the
 * alias has to be migrated before the related content entity being migrated,
 * otherwise pathauto will create an alias.
 *
 * @ReferenceDiscovery(
 *   id = "path_alias",
 *   provider = "path_alias"
 * )
 */
class PathAlias extends DiscoveryPluginBase {

  use PathAliasTrait;

  /**
   * {@inheritdoc}
   */
  public function fetchReferences(EmeObject $emeObject): array {
    $aliasIds = $this->getPathAliasIds($emeObject);
    return array_map(
      fn (string $id, string|int $revisionId): EmeObject => new EmeObject('path_alias', [
        $id,
        $revisionId,
      ]),
      $aliasIds,
      array_keys($aliasIds),
    );
  }

}
