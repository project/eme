<?php

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;
use Drupal\entity_embed\Plugin\Filter\EntityEmbedFilter;
use Drupal\filter\FilterFormatInterface;
use Drupal\filter\Plugin\FilterInterface;
use Drupal\media\Plugin\Filter\MediaEmbed;
use Drupal\text\Plugin\Field\FieldType\TextLongItem;
use Drupal\text\Plugin\Field\FieldType\TextWithSummaryItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Embed entity discovery plugin for formatted text fields.
 *
 * @ReferenceDiscovery(
 *   id = "embed_entity",
 *   provider = "filter",
 * )
 */
class EmbedEntity extends DiscoveryPluginBase {

  use DiscoveryTrait;

  const MEDIA_EMBED_PLUGIN_CLASS = MediaEmbed::class;
  // @phpstan-ignore-next-line (Entity Embed is not yet a test dependency.)
  const ENTITY_EMBED_PLUGIN_CLASS = EntityEmbedFilter::class;
  const KNOWN_EMBED_PLUGIN_CLASSES = [
    self::MEDIA_EMBED_PLUGIN_CLASS,
    self::ENTITY_EMBED_PLUGIN_CLASS,
  ];

  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager,
    protected PluginManagerInterface $filterPluginManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $entityFieldManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.filter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fetchReferences(EmeObject $emeObject): array {
    $textFields = array_merge(
      $this->getEntityFieldDefinitions($emeObject, TextLongItem::class),
      $this->getEntityFieldDefinitions($emeObject, TextWithSummaryItem::class),
    );

    if (empty($textFields)) {
      return [];
    }

    $embedFilters = array_filter(
      $this->filterPluginManager->getDefinitions(),
      function (array $definition): bool {
        foreach (static::KNOWN_EMBED_PLUGIN_CLASSES as $class) {
          if (is_a($definition['class'], $class, TRUE)) {
            return TRUE;
          }
        }
        return FALSE;
      }
    );
    if (empty($embedFilters)) {
      return [];
    }

    $filterFormatsWithEmbedFilters = array_filter(
      $this->entityTypeManager->getStorage('filter_format')->loadMultiple(),
      function (FilterFormatInterface $filter_format) use ($embedFilters) {
        $enabled_filters = array_filter(
          $filter_format->get('filters'),
          function (array $data) {
            return $data['status'];
          }
        );
        return !empty(array_intersect(
          array_keys($enabled_filters),
          array_keys($embedFilters)
        ));
      }
    );
    if (empty($filterFormatsWithEmbedFilters)) {
      return [];
    }

    $embed_entities = [];
    foreach (array_keys($textFields) as $fieldName) {
      $embed_entities = array_merge(
        $embed_entities,
        $this->getEmbedEntitiesFromField($emeObject, $fieldName, $filterFormatsWithEmbedFilters),
      );
    }

    return $embed_entities;
  }

  /**
   * Fetches embed entities from the given formatted text field's value.
   *
   * @param \Drupal\eme\EmeObject $emeObject
   *   The EME object representation of a content entity.
   * @param string $fieldName
   *   The field name.
   * @param \Drupal\filter\FilterFormatInterface[] $filterFormats
   *   List of filter formats which have at least one embed filter plugin.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   List of the embed content entities.
   */
  protected function getEmbedEntitiesFromField(EmeObject $emeObject, string $fieldName, array $filterFormats): array {
    if (!is_array($fieldValue = $this->getEntityFieldValue($emeObject, $fieldName))) {
      return [];
    }
    $filterFormatIds = array_map(
      fn (FilterFormatInterface $filterFormat): string => $filterFormat->id(),
      $filterFormats
    );
    $embedEntities = [];

    foreach ($fieldValue as $propertyValues) {
      // Assume 'format' contains the filter format.
      // @todo Fetch this precisely from field storage -> property definitions.
      if (!in_array($propertyValues['format'], $filterFormatIds, TRUE)) {
        continue;
      }

      // Assume that 'summary' or 'value' are the only properties which can
      // contain formatted text.
      foreach (['summary', 'value'] as $textProp) {
        $value = $propertyValues[$textProp] ?? NULL;
        if (empty($value)) {
          continue;
        }

        $embedEntities = array_merge(
          $embedEntities,
          $this->fetchEmbedsAsEmeObjects($value, $filterFormats[$propertyValues['format']]),
        );
      }
    }

    return $embedEntities;
  }

  /**
   * Returns the EME objects embed into the given text.
   *
   * @param string $text
   *   The text to parse.
   * @param \Drupal\filter\FilterFormatInterface $filterFormat
   *   The filter format of the text.
   *
   * @return \Drupal\eme\EmeObject[]
   *   The EME objects embed into the given text.
   */
  protected function fetchEmbedsAsEmeObjects(string $text, FilterFormatInterface $filterFormat): array {
    $enabledFilters = array_filter(
      $filterFormat->filters()->getAll(),
      fn (FilterInterface $filter): bool => $filter->getConfiguration()['status'] ?? TRUE,
    );
    $hasMediaEmbedFilters = array_reduce(
      $enabledFilters,
      function (bool $carry, FilterInterface $filter): bool {
        $carry = $carry ?: is_a($filter->getPluginDefinition()['class'], self::MEDIA_EMBED_PLUGIN_CLASS, TRUE);
        return $carry;
      },
      FALSE,
    );
    $hasEntityEmbedFilters = array_reduce(
      $enabledFilters,
      function (bool $carry, FilterInterface $filter): bool {
        $carry = $carry ?: is_a($filter->getPluginDefinition()['class'], self::ENTITY_EMBED_PLUGIN_CLASS, TRUE);
        return $carry;
      },
      FALSE,
    );
    return array_reduce(
      array_merge(
        $hasEntityEmbedFilters ? $this->getEntityEmbedObjects($text) : [],
        $hasMediaEmbedFilters ? $this->getMediaEmbedObjects($text) : [],
      ),
      function (array $carry, EmeObject $emeObject): array {
        $carry[$emeObject->getId()] = $emeObject;
        return $carry;
      },
      [],
    );
  }

  /**
   * Returns the EME objects embed into the given text by entity embed filter.
   *
   * @param string $text
   *   The text to parse.
   *
   * @return \Drupal\eme\EmeObject[]
   *   The EME objects embed into the given text.
   */
  protected function getEntityEmbedObjects(string $text): array {
    if (!str_contains($text, '<drupal-entity')) {
      return [];
    }
    // Entity embed: UUID first, then ID.
    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);

    $embeds = [];
    foreach ($xpath->query('//drupal-entity[@data-entity-type and (@data-entity-uuid or @data-entity-id)]') as $embedNode) {
      assert($embedNode instanceof \DOMElement);
      $entityTypeId = $embedNode->getAttribute('data-entity-type');
      $uuid = $embedNode->getAttribute('data-entity-uuid');
      $embeds[] = $uuid
        ? $this->emeObjectFromTypeAndUuid($entityTypeId, $uuid)
        : $this->emeObjectFromTypeAndIdentifier($entityTypeId, $embedNode->getAttribute('data-entity-id'));
    }

    return array_filter($embeds);
  }

  /**
   * Returns the EME objects embed into the given text by media embed filter.
   *
   * @param string $text
   *   The text to parse.
   *
   * @return \Drupal\eme\EmeObject[]
   *   The EME objects embed into the given text.
   */
  protected function getMediaEmbedObjects(string $text): array {
    if (!str_contains($text, '<drupal-media')) {
      return [];
    }
    // Entity embed: UUID first, then ID.
    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);

    $embeds = [];
    foreach ($xpath->query('//drupal-media[@data-entity-type="media" and normalize-space(@data-entity-uuid)!=""]') as $embedNode) {
      assert($embedNode instanceof \DOMElement);
      $embeds[] = $this->emeObjectFromTypeAndUuid(
        $embedNode->getAttribute('data-entity-type'),
        $embedNode->getAttribute('data-entity-uuid'),
      );
    }

    return array_filter($embeds);
  }

}
