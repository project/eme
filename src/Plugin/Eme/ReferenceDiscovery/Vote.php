<?php

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

// cspell:ignore votingapi
use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;

/**
 * Discovery plugin for votes associated with the current entity.
 *
 * @ReferenceDiscovery(
 *   id = "vote",
 *   provider = "votingapi",
 * )
 */
class Vote extends DiscoveryPluginBase {

  /**
   * {@inheritdoc}
   */
  public function fetchReferences(EmeObject $emeObject): array {
    $entityId = current($emeObject->getObjectIds());
    $voteIds = $this->entityTypeManager->getStorage('votingapi_vote')->getQuery()
      ->accessCheck(FALSE)
      ->condition('entity_type', $emeObject->getObjectIds())
      ->condition('entity_id', $entityId)
      ->execute();

    return array_map(
      fn (string $id): EmeObject => new EmeObject('vote', [$id]),
      $voteIds,
    );
  }

}
