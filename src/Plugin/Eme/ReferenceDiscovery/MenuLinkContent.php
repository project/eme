<?php

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;
use Drupal\path_alias\PathAliasInterface;

/**
 * Menu link content discovery plugin.
 *
 * @ReferenceDiscovery(
 *   id = "menu_link_content",
 *   provider = "menu_link_content"
 * )
 */
class MenuLinkContent extends DiscoveryPluginBase {

  use PathAliasTrait;

  /**
   * {@inheritdoc}
   */
  public function fetchReverseReferences(EmeObject $emeObject): array {
    if (!$entityInternalPath = $this->getInternalPath($emeObject)) {
      return [];
    }

    // For now, only deal with canonicals.
    // Later we might have something like this:
    // @code
    // $pattern = '{' . $entity->getEntityTypeID() . '}';
    // $entity_related_link_templates = array_filter(
    //   $entity->getEntityType()->getLinkTemplates(),
    //   function (string $link_template) use ($pattern) {
    //     return strpos($link_template, $pattern) !== FALSE;
    //   }
    // );
    // @endcode
    // @see menu_ui_get_menu_link_defaults()
    $entityTypeId = $emeObject->getPluginId();
    $entityId = current($emeObject->getObjectIds());
    $pathAliases = $this->entityTypeManager->hasDefinition('path_alias')
      ? array_reduce(
        $this->entityTypeManager->getStorage('path_alias')->loadMultiple($this->getPathAliasIds($emeObject)),
        function (array $carry, PathAliasInterface $pathAlias): array {
          $carry[] = $pathAlias->getAlias();
          return $carry;
        },
        [],
      )
      : [];
    $linkUrisToSearchFor = array_unique(array_merge(
      [
        "internal:$entityInternalPath",
        "entity:$entityTypeId/$entityId",
        "route:entity.$entityTypeId.canonical;$entityTypeId=$entityId",
      ],
      $pathAliases,
    ));

    $results = $this->entityTypeManager->getStorage('menu_link_content')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('link.uri', $linkUrisToSearchFor, 'IN')
      ->execute();

    return array_map(
      fn (string $id, string|int $revisionId): EmeObject => new EmeObject('menu_link_content', [
        $id,
        $revisionId,
      ]),
      $results,
      array_keys($results),
    );
  }

}
