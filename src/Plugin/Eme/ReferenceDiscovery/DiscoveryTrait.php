<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\eme\EmeObject;

/**
 * Trait for reference / dependency discovery plugins.
 */
trait DiscoveryTrait {

  /**
   * Returns the field definitions of the entity represented by the EME object.
   *
   * @param \Drupal\eme\EmeObject $emeObject
   *   The EME object.
   * @param string|null $itemOrListClass
   *   Field item list class or item class to filter for. Optional, returns all
   *   entity field definitions if omitted or called with NULL value.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   The field definitions of the entity represented by the EME object,
   *   optionally filtered to the given list or item classes.
   */
  protected function getEntityFieldDefinitions(EmeObject $emeObject, ?string $itemOrListClass): array {
    if (!$this->entityTypeManager->hasDefinition($emeObject->getPluginId())) {
      return [];
    }
    $entity = $this->entityTypeManager->getStorage($emeObject->getPluginId())->load(current($emeObject->getObjectIds()));
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    if (is_null($itemOrListClass)) {
      return $fieldDefinitions;
    }

    return array_filter(
      $fieldDefinitions,
      function (FieldDefinitionInterface $fieldDefinition) use ($itemOrListClass): bool {
        return is_a($fieldDefinition->getClass(), $itemOrListClass, TRUE) ||
          is_a($fieldDefinition->getItemDefinition()->getClass(), $itemOrListClass, TRUE);
      }
    );
  }

  /**
   * Returns the field value of the given EME object (if any).
   *
   * @param \Drupal\eme\EmeObject $emeObject
   *   The EME object.
   * @param string $fieldName
   *   The name of the field to get the value of.
   *
   * @return mixed
   *   The field value of the object, or NULL if the object does not represent a
   *   content entity.
   */
  protected function getEntityFieldValue(EmeObject $emeObject, string $fieldName): mixed {
    if (!$type = $this->entityTypeManager->getDefinition($emeObject->getPluginId(), FALSE)) {
      return NULL;
    }
    if (!$type instanceof ContentEntityTypeInterface) {
      return NULL;
    }
    return $this->entityTypeManager
      ->getStorage($type->id())
      ->load(current($emeObject->getObjectIds()))
      ->get($fieldName)
      ->getValue();
  }

  /**
   * Creates EME object from entity type ID and entity ID.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   * @param string|int $entityId
   *   The entity ID.
   *
   * @return \Drupal\eme\EmeObject|null
   *   The EME object, or NULL if the entity type cannot be determined.
   */
  protected function emeObjectFromTypeAndIdentifier(string $entityTypeId, string|int $entityId): ?EmeObject {
    if (!$entityType = $this->entityTypeManager->getDefinition($entityTypeId, FALSE)) {
      return NULL;
    }

    if (!$entityType->getKey('revision')) {
      return new EmeObject($entityTypeId, [$entityId]);
    }

    $results = $this->entityTypeManager->getStorage($entityTypeId)
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition($entityType->getKey('id'), $entityId)
      ->execute();

    return new EmeObject($entityTypeId, [current($results), key($results)]);
  }

  /**
   * Creates an EME object from an entity type ID and UUID.
   *
   * @param string $entityTypeId
   *   The entity type ID of the entity.
   * @param string $uuid
   *   The UUID of the entity.
   *
   * @return \Drupal\eme\EmeObject|null
   *   The EME object of the entity, or NULL if the entity cannot be identified.
   */
  protected function emeObjectFromTypeAndUuid(string $entityTypeId, string $uuid): ?EmeObject {
    $definition = $this->entityTypeManager->getDefinition($entityTypeId);
    $hasRevisions = $definition->getKey('revision');
    $uuidKey = $definition->getKey('uuid');
    $results = $this->entityTypeManager->getStorage($entityTypeId)
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition($uuidKey, $uuid)
      ->execute();
    if (!$results) {
      return NULL;
    }

    return $hasRevisions
      ? new EmeObject($entityTypeId, [current($results), key($results)])
      : new EmeObject($entityTypeId, [current($results)]);
  }

  /**
   * Creates an EME object from an entity type ID and revision ID.
   *
   * @param string $entityTypeId
   *   The entity type ID of the entity.
   * @param string|int $revisionId
   *   The revision ID of the entity.
   *
   * @return \Drupal\eme\EmeObject|null
   *   The EME object of the entity, or NULL if the entity cannot be identified.
   */
  protected function emeObjectFromTypeAndRevisionId(string $entityTypeId, string|int $revisionId): ?EmeObject {
    $revisionIdKey = $this->entityTypeManager->getDefinition($entityTypeId)->getKey('revision');
    $results = $this->entityTypeManager->getStorage($entityTypeId)
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition($revisionIdKey, $revisionId)
      ->execute();

    return $results
      ? new EmeObject($entityTypeId, [current($results), key($results)])
      : NULL;
  }

}
