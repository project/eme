<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;

/**
 * Discovery for crop entities associated with files.
 *
 * Crops have to be imported before files, since Crop API creates crop entities
 * on file save. This behavior makes it impossible to "change" crops after a
 * crop migration rollback and reimport.
 *
 * @ReferenceDiscovery(
 *   id = "crop",
 *   provider = "crop"
 * )
 */
class Crop extends DiscoveryPluginBase {

  /**
   * {@inheritdoc}
   */
  public function fetchReferences(EmeObject $emeObject): array {
    $results = $this->entityTypeManager->getStorage('crop')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('entity_type', $emeObject->getPluginId())
      ->condition('entity_id', current($emeObject->getObjectIds()))
      ->execute();

    return array_map(
      fn (string $id, string|int $revisionId): EmeObject => new EmeObject('crop', [
        $id,
        $revisionId,
      ]),
      $results,
      array_keys($results)
    );
  }

}
