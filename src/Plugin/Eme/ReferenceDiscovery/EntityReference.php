<?php

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;

/**
 * Entity reference discovery plugin for entity reference based fields.
 *
 * @ReferenceDiscovery(
 *   id = "entity_reference"
 * )
 */
class EntityReference extends DiscoveryPluginBase {

  use DiscoveryTrait;

  /**
   * {@inheritdoc}
   */
  public function fetchReferences(EmeObject $emeObject): array {
    $referenceFields = $this->getEntityFieldDefinitions($emeObject, EntityReferenceFieldItemListInterface::class);

    if (!$referenceFields) {
      return [];
    }

    $objects = [];
    $entity = $this->entityTypeManager->getStorage($emeObject->getPluginId())->load(current($emeObject->getObjectIds()));
    foreach (array_keys($referenceFields) as $fieldName) {
      foreach ($entity->get($fieldName)->referencedEntities() as $referencedEntity) {
        if ($referencedEntity instanceof ContentEntityInterface) {
          $objects[] = EmeObject::createFromContentEntity($referencedEntity);
        }
      }
    }

    return $objects;
  }

}
