<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\comment\CommentStatisticsInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Discovers comments entity statistics records of the current content entity.
 *
 * @ReferenceDiscovery(
 *   id = "comment_entity_statistics",
 *   provider = "comment"
 * )
 */
class CommentEntityStatistics extends DiscoveryPluginBase {

  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager,
    protected CommentStatisticsInterface $commentStatistics,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $entityFieldManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('comment.statistics'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fetchReverseReferences(EmeObject $emeObject): array {
    $entityId = current($emeObject->getObjectIds());
    return array_map(
      fn (\stdClass $object): EmeObject => new EmeObject('comment_entity_statistics', [
        $object->entity_id,
        $object->entity_type,
        $object->field_name,
      ]),
      $this->commentStatistics->read([$entityId => $entityId], $emeObject->getPluginId()),
    );
  }

}
