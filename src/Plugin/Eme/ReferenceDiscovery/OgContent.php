<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;
use Drupal\og\OgGroupAudienceHelperInterface;

/**
 * Organic group content discovery plugin.
 *
 * @ReferenceDiscovery(
 *   id = "og_content",
 *   provider = "og",
 * )
 */
class OgContent extends DiscoveryPluginBase {

  /**
   * {@inheritdoc}
   */
  public function fetchReverseReferences(EmeObject $emeObject): array {
    $groupContentItems = [];
    foreach ($this->getContentEntityTypeIds() as $entityTypeId => $definition) {
      $storage = $this->entityTypeManager->getStorage($entityTypeId);
      if (!$storage instanceof SqlContentEntityStorage) {
        continue;
      }
      $isRevisionable = !empty($definition->getKey('revision'));
      try {
        $entityIds = $storage->getQuery()
          ->accessCheck(FALSE)
          // @phpstan-ignore-next-line (OG is not marked as test dependency.)
          ->condition(OgGroupAudienceHelperInterface::DEFAULT_FIELD, current($emeObject->getObjectIds()))
          ->execute();
        if (!$entityIds) {
          continue;
        }
        $objects = array_map(
          function ($id, $revisionId) use ($isRevisionable, $entityTypeId): EmeObject {
            $params = $isRevisionable
              ? [(string) $id, (string) $revisionId]
              : [(string) $id];
            return new EmeObject($entityTypeId, $params);
          },
          $entityIds,
          array_keys($entityIds),
        );
        $groupContentItems += array_reduce(
          $objects,
          function (array $carry, EmeObject $object): array {
            $carry[$object->getId()] = $object;
            return $carry;
          },
          [],
        );
      }
      catch (\Exception) {
      }
    }

    return $groupContentItems;
  }

  /**
   * Returns every available content entity type definition.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   *   All content entity type definition, keyed by their entity type ID.
   */
  protected function getContentEntityTypeIds(): array {
    return array_keys(
      array_filter(
        $this->entityTypeManager->getDefinitions(),
        function (EntityTypeInterface $entityType): bool {
          return $entityType instanceof ContentEntityTypeInterface;
        }
      )
    );
  }

}
