<?php

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;
use Drupal\layout_builder\Field\LayoutSectionItemList;
use Drupal\layout_builder\Section;

/**
 * Discovery plugin for entities used in layout override field values.
 *
 * @ReferenceDiscovery(
 *   id = "layout_override",
 *   provider = "layout_builder"
 * )
 */
class LayoutOverride extends DiscoveryPluginBase implements ContainerFactoryPluginInterface {

  use DiscoveryTrait;

  /**
   * {@inheritdoc}
   */
  public function fetchReferences(EmeObject $emeObject): array {
    $layoutFields = $this->getEntityFieldDefinitions($emeObject, LayoutSectionItemList::class);

    if (empty($layoutFields)) {
      return [];
    }

    $objects = [];
    $entity = $this->entityTypeManager->getStorage($emeObject->getPluginId())->load(current($emeObject->getObjectIds()));
    foreach (array_keys($layoutFields) as $fieldName) {
      $field = $entity->get($fieldName);
      foreach ($field->getSections() as $section) {
        assert($section instanceof Section);
        foreach ($section->getComponents() as $component) {
          $config = (array) $component->get('configuration');

          switch ($config['provider'] ?? NULL) {
            case 'block_content':
              // This section component is a reusable block content entity.
              $carry[] = $this->emeObjectFromTypeAndUuid('block_content', preg_replace('/^block_content:/', '', $config['id']));
              break;

            case 'layout_builder':
              if (strpos($config['id'], 'inline_block:') === 0) {
                // This section component is an inline (non-reusable) block
                // content entity.
                $carry[] = $this->emeObjectFromTypeAndRevisionId('block_content', $config['block_revision_id']);
              }
              break;
          }
        }
      }
    }

    return $objects;
  }

}
