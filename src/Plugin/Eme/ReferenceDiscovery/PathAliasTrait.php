<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\eme\EmeObject;

/**
 * Trait for discovering path aliases.
 */
trait PathAliasTrait {

  /**
   * Returns the internal path of the object, if any.
   *
   * @param \Drupal\eme\EmeObject $emeObject
   *   The EME object.
   *
   * @return string|null
   *   The internal path of the object, or NULL if the entity has no path.
   */
  protected function getInternalPath(EmeObject $emeObject): ?string {
    $entityType = $this->entityTypeManager->getDefinition($emeObject->getPluginId(), FALSE);
    // Early opt-out.
    if (!$entityType || !$linkTemplate = $entityType->getLinkTemplate('canonical')) {
      return NULL;
    }

    return preg_replace(
      "/{{$entityType->id()}}/",
      current($emeObject->getObjectIds()),
      $linkTemplate,
    );
  }

  /**
   * Returns the IDs of the object's path aliases.
   *
   * @param \Drupal\eme\EmeObject $emeObject
   *   The EME object.
   *
   * @return string[]
   *   The IDs of the object's path aliases.
   */
  protected function getPathAliasIds(EmeObject $emeObject): array {
    if (!$internalPath = $this->getInternalPath($emeObject)) {
      return [];
    }
    return $this->entityTypeManager->getStorage('path_alias')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('path', $internalPath)
      ->execute();
  }

}
