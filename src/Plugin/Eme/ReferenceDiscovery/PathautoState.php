<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Pathauto state discovery plugin.
 *
 * @ReferenceDiscovery(
 *   id = "pathauto_state",
 *   provider = "pathauto"
 * )
 */
class PathautoState extends DiscoveryPluginBase {

  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager,
    protected KeyValueFactoryInterface $keyvalueFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $entityFieldManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('keyvalue'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fetchReverseReferences(EmeObject $emeObject): array {
    $entityTypeId = $emeObject->getPluginId();
    $entityId = current($emeObject->getObjectIds());
    $pathAliasState = $this->keyvalueFactory->get("pathauto_state.$entityTypeId")->get($entityId);
    $emeObject = isset($pathAliasState)
      ? new EmeObject('pathauto_state', [$entityTypeId, $entityId])
      : NULL;

    return array_filter([$emeObject]);
  }

}
