<?php

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;

/**
 * Flagging discovery plugin.
 *
 * @ReferenceDiscovery(
 *   id = "flagging",
 *   provider = "flag"
 * )
 */
class Flagging extends DiscoveryPluginBase {

  /**
   * {@inheritdoc}
   */
  public function fetchReverseReferences(EmeObject $emeObject): array {
    // Perform the query of FlagServiceInterface::getAllEntityFlaggings()
    // directly, and without any access check.
    $flaggingIds = $this->entityTypeManager->getStorage('flagging')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('entity_type', $emeObject->getPluginId())
      ->condition('entity_id', current($emeObject->getObjectIds()))
      ->execute();
    return array_map(
      fn (string $id): EmeObject => new EmeObject('flagging', [$id]),
      $flaggingIds,
    );
  }

}
