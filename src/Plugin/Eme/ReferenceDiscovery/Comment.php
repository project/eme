<?php

declare(strict_types=1);

namespace Drupal\eme\Plugin\Eme\ReferenceDiscovery;

use Drupal\eme\EmeObject;
use Drupal\eme\ReferenceDiscovery\DiscoveryPluginBase;

/**
 * Discovers comments sent to the current content entity.
 *
 * @ReferenceDiscovery(
 *   id = "comment",
 *   provider = "comment"
 * )
 */
class Comment extends DiscoveryPluginBase {

  /**
   * {@inheritdoc}
   */
  public function fetchReverseReferences(EmeObject $emeObject): array {
    $ids = $this->entityTypeManager->getStorage('comment')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('entity_type', $emeObject->getPluginId())
      ->condition('entity_id', current($emeObject->getObjectIds()))
      ->execute();
    return array_map(
      fn (string $id): EmeObject => new EmeObject('comment', [$id]),
      array_values($ids),
    );
  }

}
