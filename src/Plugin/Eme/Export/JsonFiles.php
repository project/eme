<?php

namespace Drupal\eme\Plugin\Eme\Export;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\eme\EmeExportable\ExportablePluginInterface;
use Drupal\eme\EmeObject;
use Drupal\eme\Export\ExportPluginBase;
use Drupal\eme\ExportException;
use Drupal\eme\Plugin\Eme\Exportable\File as FileExporter;
use Drupal\eme\Utility\EmeModuleFileUtils;
use Drupal\file\Entity\File;

/**
 * Export plugin for migrations with local JSON file source.
 *
 * @Export(
 *   id = "json_files",
 *   label = @Translation("JSON files source"),
 *   description = @Translation("The exported migration contains the source data as well.")
 * )
 */
class JsonFiles extends ExportPluginBase {

  /**
   * The data source directory.
   *
   * @const string
   */
  const DATA_SUBDIR = 'data';

  /**
   * The directory of the files.
   *
   * @const string
   */
  const FILE_ASSETS_SUBDIR = 'assets';

  /**
   * Data file extension.
   *
   * @var string
   */
  protected $dataFileExtension = 'json';

  /**
   * {@inheritdoc}
   */
  public function tasks(): array {
    return [
      'initializeExport',
      'discoverContentReferences',
      'writeEntityDataSource',
      'writeMigratedFiles',
      'writeMigrationPlugins',
      'buildModule',
      'finishExport',
    ];
  }

  /**
   * Writes entity field values to a JSON file in the export module.
   *
   * @param \ArrayAccess $context
   *   The batch context.
   */
  protected function writeEntityDataSource(\ArrayAccess &$context): void {
    $sandbox = &$context['sandbox'];
    if (!isset($sandbox['entities_to_process'])) {
      $this->sendMessage($this->t('Collecting and writing entity data sources to files.'), $context);
      $sandbox['entities_to_process'] = $context['results']['discovered'];
      $sandbox['total'] = count($sandbox['entities_to_process'] ?? []);
      $sandbox['progress'] = 0;
    }

    $this->sendMessage($this->t('Collecting and writing entity data source to files: (@processed/@total)', [
      '@processed' => $sandbox['progress'],
      '@total' => $sandbox['total'],
    ]), $context);

    if (empty($sandbox['entities_to_process'])) {
      $context['finished'] = 1;
      return;
    }

    $current = array_shift($sandbox['entities_to_process']);
    $sandbox['progress'] += 1;
    $eme_object = EmeObject::createFromId($current);
    $exportable = $this->exportablePluginManager->getInstanceForEmeObject($eme_object);
    $entity_type_id = $exportable->getEntityTypeId();
    $bundle = $exportable->getBundle();

    // Write data.
    $this->temporaryExport()->addFileWithContent(
      $this->getDataPath($entity_type_id, $bundle, $exportable->getObjectId()),
      $this->sourceDataToFileContent($exportable->getValues())
    );

    if ($bundle) {
      $context['results']['exported_entities'][$entity_type_id][$bundle][] = $current;
    }
    else {
      $context['results']['exported_entities'][$entity_type_id][] = $current;
    }

    if ($sandbox['progress'] < $sandbox['total']) {
      $context['finished'] = $sandbox['progress'] / $sandbox['total'];
      $this->flushEntityMemoryCache($sandbox['progress']);
    }
    else {
      $context['finished'] = 1;
      $this->flushEntityMemoryCache();
    }
  }

  /**
   * Adds files required for file migrations to the export module.
   *
   * @param \ArrayAccess $context
   *   The batch context.
   */
  protected function writeMigratedFiles(\ArrayAccess &$context): void {
    $sandbox = &$context['sandbox'];
    if (!isset($sandbox['total'])) {
      $files = array_values($context['results']['exported_entities']['file'] ?? []);
      $sandbox['files_to_process'] = array_combine($files, $files);
      $sandbox['total'] = count($sandbox['files_to_process']);
      $sandbox['progress'] = 0;
      if (empty($sandbox['files_to_process'])) {
        $context['finished'] = 1;
        return;
      }
      $this->sendMessage($this->t('Copy the necessary files.'), $context);
    }

    $this->sendMessage($this->t('Copy the necessary files: (@processed/@total)', [
      '@processed' => $sandbox['progress'],
      '@total' => $sandbox['total'],
    ]), $context);

    // Add file to the archive.
    $eme_object_id = array_shift($sandbox['files_to_process']);
    $exportable = $this->exportablePluginManager->getInstanceForEmeObject(EmeObject::createFromId($eme_object_id));
    assert($exportable instanceof FileExporter);
    $current_file_id = $exportable->getObjectId();
    $file = $this->entityTypeManager->getStorage('file')
      ->load($current_file_id);
    if ($file instanceof File) {
      $file_uri = $file->getFileUri();
      $scheme = StreamWrapperManager::getScheme($file_uri);
      $this->temporaryExport()->addFiles([$file_uri], self::getFileDirectory($scheme), $scheme . '://');
    }
    $sandbox['progress'] += 1;

    if ($sandbox['progress'] < $sandbox['total']) {
      $context['finished'] = $sandbox['progress'] / $sandbox['total'];
      $this->flushEntityMemoryCache($sandbox['progress']);
    }
    else {
      $context['finished'] = 1;
      $this->flushEntityMemoryCache();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function createMigrationPluginDefinition(ExportablePluginInterface $exportable, array $results): array {
    $definition = parent::createMigrationPluginDefinition($exportable, $results);
    $entity_type_id = $exportable->getEntityTypeId();
    $bundle = $exportable->getBundle();
    $exported_entities = $results['exported_entities'] ?? [];

    // JsonFileSource creates a single file for every exported entity.
    $eme_object_ids = $bundle
      ? array_values($exported_entities[$entity_type_id][$bundle])
      : array_values($exported_entities[$entity_type_id]);
    $entity_ids = array_map(
      function (string $eme_object_id) {
        return $this->exportablePluginManager->getInstanceForEmeObject(
          EmeObject::createFromId($eme_object_id)
        )->getObjectId();
      },
      $eme_object_ids
    );
    $urls = array_reduce(
      $entity_ids,
      function (array $carry, $entity_id) use ($entity_type_id, $bundle) {
        $carry[] = implode('/', [
          '..',
          $this->getDataPath($entity_type_id, $bundle, $entity_id),
        ]);
        return $carry;
      },
      []
    );
    natsort($urls);

    $definition['source'] = [
      'plugin' => 'url',
      'data_fetcher_plugin' => 'file',
      'item_selector' => '/',
      'data_parser_plugin' => 'json',
      'urls' => array_values($urls),
      'ids' => $exportable->getObjectIdentifierKeysAndTypes(),
    ];

    foreach (array_keys($definition['process']) as $field_name) {
      $definition['source']['fields'][$field_name] = [
        'name' => $field_name,
        'selector' => '/' . $field_name,
      ];
    }

    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildModule(\ArrayAccess &$context): void {
    parent::buildModule($context);

    $module = $this->configuration['module'];
    $module_list = $this->moduleExtensionList->reset()->getList();

    if (array_key_exists($module, $module_list)) {
      $module_path = $module_list[$module]->getPath();
      // Delete data source directory.
      $data_dir = implode('/', [
        $module_path,
        self::DATA_SUBDIR,
      ]);
      if (file_exists($data_dir)) {
        $this->fileSystem->deleteRecursive($data_dir);
      }

      // Delete file assets directory.
      $file_dir = implode('/', [
        $module_path,
        self::FILE_ASSETS_SUBDIR,
      ]);
      if (file_exists($file_dir)) {
        $this->fileSystem->deleteRecursive($file_dir);
      }
    }

    // Ensure that the required migration plugin alter hook is properly used.
    $module_content = $this->temporaryExport()->getFileContent("$module.module") ?? EmeModuleFileUtils::getBareModuleFile($module);

    // Add the migration plugin alterer class.
    $this->temporaryExport()->addFileWithContent(
      'src/MigrationPluginAlterer.php',
      self::migrationPluginAltererClass($module)
    );
    EmeModuleFileUtils::ensureUseDeclaration(
      "Drupal\\{$module}\\MigrationPluginAlterer",
      $module_content
    );
    EmeModuleFileUtils::ensureFunctionUsedInHook(
      'hook_migration_plugins_alter',
      ['&$definitions'],
      'MigrationPluginAlterer::alterDefinitions($1)',
      $module,
      $module_content
    );
    $this->temporaryExport()->addFileWithContent("{$module}.module", $module_content);

    // The migration source plugin is provided by Migrate Plus.
    $info_yaml = $this->temporaryExport()->getFileContent("$module.info.yml");
    $info = Yaml::decode($info_yaml);
    $dependencies = array_unique(
      array_merge(
        $info['dependencies'] ?? [],
        ['migrate_plus:migrate_plus']
      )
    );
    natsort($dependencies);
    $info['dependencies'] = array_values($dependencies);
    $this->temporaryExport()->addFileWithContent("$module.info.yml", Yaml::encode($info));

    $context['finished'] = 1;
  }

  /**
   * Converts an entity data array to a file content.
   *
   * @param array[] $entity_revisions
   *   An array of array with the entity (revision) values.
   *
   * @return string
   *   The content of the data file.
   */
  protected function sourceDataToFileContent(array $entity_revisions): string {
    $file_content = json_encode($entity_revisions, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_THROW_ON_ERROR);

    if ($file_content === FALSE) {
      throw new ExportException();
    }

    return $file_content . "\n";
  }

  /**
   * Returns the directory where the data source of an entity should be saved.
   *
   * @param string $entity_type_id
   *   The entity type ID of the entity, e.g. "node".
   * @param string|null $bundle
   *   The bundle ID of the entity, e.g. "article".
   *
   * @return string
   *   The directory where the data source of an entity should be saved.
   */
  private static function getDataDirectory(string $entity_type_id, string $bundle = NULL): string {
    return implode('/', array_filter([
      self::DATA_SUBDIR,
      $entity_type_id,
      $bundle,
    ]));
  }

  /**
   * Returns the path where the data of the specified entity should be saved.
   *
   * @param string $entity_type_id
   *   The entity type ID of the entity, e.g. "node".
   * @param string|null $bundle
   *   The bundle ID of the entity, e.g. "article".
   * @param string|int $entity_id
   *   The ID of the entity.
   *
   * @return string
   *   The full path where the data of the specified entity should be saved.
   */
  private function getDataPath(string $entity_type_id, $bundle, $entity_id): string {
    // Only use word chars in file name.
    $entity_id_safe = preg_replace('/[^\w\.-]/', '_', $entity_id);
    return implode('/', [
      self::getDataDirectory($entity_type_id, $bundle),
      "{$entity_type_id}-{$entity_id_safe}.{$this->dataFileExtension}",
    ]);
  }

  /**
   * Returns the directory where files with a specified scheme should be saved.
   *
   * @param string|false $scheme
   *   A scheme.
   *
   * @return string
   *   The directory where files with the specified scheme should be saved;
   *   relative to the generated module's root.
   */
  public static function getFileDirectory($scheme = FALSE): string {
    return implode('/', array_filter([
      self::FILE_ASSETS_SUBDIR,
      $scheme,
    ]));
  }

  /**
   * Returns the content of the migration plugin alterer class.
   *
   * @param string $module_name
   *   The name of the export module.
   *
   * @return string
   *   The content of the migration plugin alterer class.
   */
  protected static function migrationPluginAltererClass(string $module_name): string {
    return <<<EOF
<?php

namespace Drupal\\$module_name;

/**
 * Alters the migration plugin definitions.
 */
class MigrationPluginAlterer {

  /**
   * Alters the migration plugin definitions.
   */
  public static function alterDefinitions(&\$definitions) {
    \$directory_separator = preg_quote(DIRECTORY_SEPARATOR, '/');
    \$module_root = preg_replace('/' . \$directory_separator . 'src$/', '', __DIR__);

    // Update source references in our migrations.
    foreach (\$definitions as \$plugin_id => \$definition) {
      if (\$definition['provider'] !== '$module_name') {
        continue;
      }
      // Set constant for file migration.
      \$definitions[\$plugin_id]['source']['constants']['eme_file_path'] = implode(DIRECTORY_SEPARATOR, [
        \$module_root,
        'assets',
      ]);

      // Set the real path to the data source assets.
      if (!empty(\$definitions[\$plugin_id]['source']['urls'])) {
        \$source_urls = \$definitions[\$plugin_id]['source']['urls'];
        assert(is_array(\$source_urls));
        foreach (\$source_urls as \$key => \$source_url) {
          assert(is_string(\$source_url));
          \$definitions[\$plugin_id]['source']['urls'][\$key] = str_replace(
            '..',
            \$module_root,
            \$source_url
          );
        }
      }
    }
  }

}

EOF;
  }

}
