<?php

declare(strict_types=1);

namespace Drupal\Tests\eme\Functional;

use Drupal\Core\Extension\ModuleInstallerInterface;

/**
 * Tests coverage for "all" argument, bundle and entity ID filters.
 *
 * @group eme
 */
class DrushFilteredExportTest extends DrushOperationsTest {

  /**
   * Tests various types of export arguments and bundle filters.
   *
   * @param array $arguments
   *   List of arguments to pass to the export command.
   * @param array $options
   *   Command line options.
   * @param array $expected
   *   The expected generated migration "base" IDs (without the migration
   *   prefix) and the number of the items in the migration.
   *
   * @dataProvider providerTestExportDrush
   */
  public function testExportDrush(array $arguments = [], array $options = [], array $expected = []): void {
    // Let's export the test content.
    $this->drush(
      'eme:export',
      $arguments,
      [
        'destination' => $this->getMigrateExportDestination(),
        'module' => $this->moduleName,
        'name' => $this->moduleHumanName,
        'id-prefix' => $this->migrationPrefix,
        'group' => $this->migrationGroup,
      ] + $options,
    );
    $this->assertOutputEquals('🎉 Export finished.');

    $moduleInstaller = $this->container->get('module_installer');
    $this->assertInstanceOf(ModuleInstallerInterface::class, $moduleInstaller);
    $moduleInstaller->install([$this->moduleName]);

    $this->drush('migrate:status', [], array_filter([
      'tag' => $this->migrationGroup,
      'fields' => 'id,status,total,imported,unprocessed',
    ]));
    $this->assertDrushOutputHasAllLines(
      $expectedLines = array_map(
        fn (string $migrationIdBase, int $itemNumber): string => "{$this->migrationPrefix}_$migrationIdBase Idle $itemNumber 0 $itemNumber",
        array_keys($expected),
        $expected,
      ),
    );

    // Reexport.
    $this->drush('eme:export', [], ['update' => $this->moduleName]);
    $this->assertOutputEquals('🎉 Export finished.');

    $this->drush('cache:rebuild');

    $this->drush(
      'migrate:status',
      [],
      [
        'tag' => $this->migrationGroup,
        'fields' => 'id,status,total,imported,unprocessed',
      ],
    );
    $this->assertDrushOutputHasAllLines($expectedLines);
  }

  /**
   * Test data provider for ::testExportDrush.
   *
   * @return array[]
   *   The test cases.
   */
  public static function providerTestExportDrush(): array {
    return [
      'Export everything' => [
        'arguments' => ['--all'],
        'options' => [],
        'expected' => [
          'comment_article_comments' => 2,
          'comment_entity_statistics' => 1,
          'file' => 2,
          'media_image' => 1,
          'menu_link_content' => 1,
          'node_article' => 1,
          'node_page' => 1,
          'path_alias' => 2,
          'pathauto_state_media' => 1,
          'pathauto_state_node' => 2,
          'pathauto_state_user' => 3,
          'user' => 4,
        ],
      ],
      'Export specific bundles' => [
        'arguments' => [],
        'options' => ['types' => 'node:article'],
        'expected' => [
          'comment_article_comments' => 2,
          'comment_entity_statistics' => 1,
          'file' => 1,
          'media_image' => 1,
          'menu_link_content' => 1,
          'node_article' => 1,
          'path_alias' => 1,
          'pathauto_state_media' => 1,
          'pathauto_state_node' => 1,
          'pathauto_state_user' => 2,
          'user' => 2,
        ],
      ],
      'Export a single node' => [
        'arguments' => [],
        'options' => ['types' => 'node:1'],
        'expected' => [
          'node_page' => 1,
          'path_alias' => 1,
          'pathauto_state_node' => 1,
          'pathauto_state_user' => 1,
          'user' => 1,
        ],
      ],
    ];
  }

}
