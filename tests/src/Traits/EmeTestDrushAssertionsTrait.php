<?php

namespace Drupal\Tests\eme\Traits;

use Drush\Drush;
use Drush\TestTraits\DrushTestTrait;
use PHPUnit\Framework\ExpectationFailedException;

/**
 * Drush specific assertions.
 */
trait EmeTestDrushAssertionsTrait {

  use DrushTestTrait;

  /**
   * Whether the actual Drush version is an old one without migrate commands.
   *
   * @return bool
   *   Whether the actual Drush version is an old one without migrate commands.
   */
  public static function isOldDrushVersion(): bool {
    return version_compare(Drush::getVersion(), '10.4.0', 'lt');
  }

  /**
   * Checks that migrate status output has all the lines.
   *
   * @param string|string[] $expected_lines
   *   The expected lines in drush output.
   * @param int|null $lines
   *   The expected number of the meaningful lines. Defaults to the length of
   *   the array at the first argument.
   */
  protected function assertDrushOutputHasAllLines($expected_lines, ?int $lines = NULL) {
    $actual_output = $this->getSimplifiedOutput();
    $actual_output_array_cleaned = array_filter(
      explode(PHP_EOL, $actual_output),
      function (string $line): bool {
        // Drop empty lines and lines which only contain dashes and spaces.
        $indifferent = $line === '' || preg_match('/^[-\s]*$/', $line);
        // Drop also those lines which are starting with "Migration ID " or with
        // "Tag: ".
        return !$indifferent && !str_starts_with($line, 'Migration ID') && !str_starts_with($line, 'Tag: ');
      }
    );
    $lines = $lines ?? count($expected_lines);

    if ($lines !== count($actual_output_array_cleaned)) {
      // Use an equals check which will fail, but also makes it easy to check
      // the differences.
      $this->assertEquals(
        implode("\n", (array) $expected_lines),
        $this->getOutput(),
        sprintf(
          "Expected %s lines, found %s.",
          $lines,
          count($actual_output_array_cleaned),
        ),
      );
    }

    try {
      foreach ((array) $expected_lines as $expected_line) {
        $expected_line_pieces = array_filter(
          explode(' ', $expected_line),
          function ($word) {
            return (string) $word !== '';
          }
        );
        foreach ($expected_line_pieces as $key => $text) {
          $expected_line_pieces[$key] = preg_quote((string) $text, '/');
        }
        $pattern = '/\b' . implode('\s+', $expected_line_pieces) . '\b/';
        $this->assertEquals(1, preg_match($pattern, $actual_output));
      }
    }
    catch (ExpectationFailedException) {
      $this->assertEquals(
        implode("\n", $expected_lines),
        $this->getOutput(),
        "Drush output lines don't match."
      );
    }
  }

}
