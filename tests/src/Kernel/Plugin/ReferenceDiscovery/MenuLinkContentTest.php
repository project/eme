<?php

namespace Drupal\Tests\eme\Kernel\Plugin\ReferenceDiscovery;

use Drupal\eme\EmeObject;
use Drupal\eme\Plugin\Eme\ReferenceDiscovery\MenuLinkContent;
use Drupal\menu_link_content\Entity\MenuLinkContent as MenuLinkContentEntity;

/**
 * Tests the menu link content reference discovery plugin.
 *
 * @coversDefaultClass \Drupal\eme\Plugin\Eme\ReferenceDiscovery\MenuLinkContent
 * @group eme
 */
class MenuLinkContentTest extends ReferenceDiscoveryPluginTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'link',
    'menu_link_content',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('menu_link_content');
  }

  /**
   * Tests that menu link content related to an entity get discovered.
   *
   * @covers ::fetchReverseReferences
   * @dataProvider providerTestFetchReverseReferences
   */
  public function testFetchReverseReferences(array $link_uris, array $expected_matching_uris): void {
    foreach ($link_uris as $uri) {
      $link = MenuLinkContentEntity::create([
        'link' => ['uri' => $uri],
        'enabled' => 1,
      ]);
      $link->save();
    }

    $plugin = new MenuLinkContent(
      [],
      'menu_link_content',
      [],
      $this->container->get('entity_type.manager'),
      $this->container->get('entity_field.manager'),
    );

    $actual_matching_menu_link_uris = array_map(
      function (EmeObject $eme_menu_link) {
        $menu_link = MenuLinkContentEntity::load($eme_menu_link->getObjectIds()[0]);
        return $menu_link->link->first()->getValue()['uri'] ?? NULL;
      },
      $plugin->fetchReverseReferences(EmeObject::createFromContentEntity($this->entity)),
    );

    sort($expected_matching_uris);
    sort($actual_matching_menu_link_uris);

    $this->assertEquals($expected_matching_uris, $actual_matching_menu_link_uris);
  }

  /**
   * Data provider for testFetchReverseReferences.
   *
   * @return array
   *   The test cases.
   */
  public static function providerTestFetchReverseReferences(): array {
    return [
      'No menu links' => [
        'link_uris' => [],
        'expected_matching_uris' => [],
      ],
      'Menu links for test entity' => [
        'link_uris' => [
          'internal:/entity_test/1',
          'entity:entity_test/1',
          'internal:/entity_test/1',
          'route:entity.entity_test.canonical;entity_test=1',
        ],
        'expected_matching_uris' => [
          'entity:entity_test/1',
          'internal:/entity_test/1',
          'internal:/entity_test/1',
          'route:entity.entity_test.canonical;entity_test=1',
        ],
      ],
      'Menu links for other test entity' => [
        'link_uris' => [
          'entity:entity_test/2',
          'internal:/entity_test/2',
          'base:entity_test/2',
          'route:entity.entity_test.canonical;entity_test=2',
        ],
        'expected_matching_uris' => [],
      ],
      'Menu links for test entity and for other entities' => [
        'link_uris' => [
          'entity:entity_test/1',
          'internal:/entity_test/2',
          'base:entity_test/2',
        ],
        'expected_matching_uris' => [
          'entity:entity_test/1',
        ],
      ],
    ];
  }

}
