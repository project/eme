<?php

namespace Drupal\Tests\eme\Kernel\Plugin\ReferenceDiscovery;

use Drupal\eme\EmeObject;
use Drupal\eme\Plugin\Eme\ReferenceDiscovery\PathAlias;
use Drupal\path_alias\Entity\PathAlias as PathAliasEntity;

/**
 * Tests the path alias reference discovery plugin.
 *
 * @coversDefaultClass \Drupal\eme\Plugin\Eme\ReferenceDiscovery\PathAlias
 * @group eme
 */
class PathAliasTest extends ReferenceDiscoveryPluginTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'path_alias',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('path_alias');
  }

  /**
   * Tests that path aliases related to an entity get discovered.
   *
   * @covers ::fetchReferences
   * @dataProvider providerTestFetchReferences
   */
  public function testFetchReferences(array $source_aliases, array $expected_aliases): void {
    foreach ($source_aliases as $data) {
      PathAliasEntity::create($data)->save();
    }

    $plugin = new PathAlias(
      [],
      'path_alias',
      [],
      $this->container->get('entity_type.manager'),
      $this->container->get('entity_field.manager'),
    );

    $actual_aliases = array_map(
      function (EmeObject $eme_path_alias) {
        $path_alias = PathAliasEntity::load($eme_path_alias->getObjectIds()[0]);
        return [
          'alias' => $path_alias->getAlias(),
          'path' => $path_alias->getPath(),
        ];
      },
      $plugin->fetchReferences(EmeObject::createFromContentEntity($this->entity)),
    );

    $this->assertEquals($expected_aliases, array_values($actual_aliases));
  }

  /**
   * Data provider for testFetchReferences.
   *
   * @return array
   *   The test cases.
   */
  public static function providerTestFetchReferences(): array {
    return [
      'No aliases' => [
        'source_aliases' => [],
        'expected_aliases' => [],
      ],
      'Path aliases for test entity' => [
        'source_aliases' => [
          ['path' => '/entity_test/1', 'alias' => '/an-alias'],
          ['path' => '/entity_test/1', 'alias' => '/yet/another/alias'],
        ],
        'expected_aliases' => [
          ['path' => '/entity_test/1', 'alias' => '/an-alias'],
          ['path' => '/entity_test/1', 'alias' => '/yet/another/alias'],
        ],
      ],
      'Unrelated aliases for other test entity' => [
        'source_aliases' => [
          ['path' => '/entity_test/2', 'alias' => '/an-unrelated-alias'],
          ['path' => '/user/1', 'alias' => '/root-user'],
        ],
        'expected_aliases' => [],
      ],
      'Aliases for the test entity and for other entities' => [
        'source_aliases' => [
          ['path' => '/entity_test/2', 'alias' => '/an-unrelated-alias'],
          ['path' => '/entity_test/1', 'alias' => '/an-alias'],
          ['path' => '/user/1', 'alias' => '/root-user'],
          ['path' => '/user/login', 'alias' => '/not-an-entity-alias'],
          ['path' => '/entity_test/1', 'alias' => '/yet/another/alias'],
        ],
        'expected_aliases' => [
          ['path' => '/entity_test/1', 'alias' => '/an-alias'],
          ['path' => '/entity_test/1', 'alias' => '/yet/another/alias'],
        ],
      ],
    ];
  }

}
