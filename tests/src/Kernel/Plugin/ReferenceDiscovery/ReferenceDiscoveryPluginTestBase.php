<?php

declare(strict_types=1);

namespace Drupal\Tests\eme\Kernel\Plugin\ReferenceDiscovery;

use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\comment\Tests\CommentTestTrait;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Base class for reference discovery plugin tests.
 */
abstract class ReferenceDiscoveryPluginTestBase extends KernelTestBase {

  use CommentTestTrait;
  use UserCreationTrait;

  protected const COMMENT_TYPE = 'default';
  protected const COMMENT_FIELD = 'comment_field';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'user',
  ];

  /**
   * The test entity.
   *
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $entity;

  /**
   * Another test entity.
   *
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $otherEntity;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test');

    if ($this->container->get('module_handler')->moduleExists('comment')) {
      $this->installEntitySchema('comment');
      $this->installConfig(['filter', 'comment']);
      $this->installSchema('comment', ['comment_entity_statistics']);
      $this->addDefaultCommentField(
        'entity_test',
        'entity_test',
        static::COMMENT_FIELD,
        CommentItemInterface::OPEN,
        static::COMMENT_TYPE
      );
      $this->container->get('state')->set('comment.maintain_entity_statistics', TRUE);
    }

    // We need an anonymous user.
    // @see https://drupal.org/i/3056234
    $uid = $this->createUser([], '', FALSE, ['uid' => 0])->save();

    // The test entity.
    $this->entity = EntityTest::create([
      'id' => 1,
      'uid' => $uid,
    ]);
    $this->entity->save();

    // Create another test entity.
    $this->otherEntity = EntityTest::create([
      'id' => 2,
      'uid' => $uid,
    ]);
    $this->otherEntity->save();
  }

}
