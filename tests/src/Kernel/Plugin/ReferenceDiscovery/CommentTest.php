<?php

declare(strict_types=1);

namespace Drupal\Tests\eme\Kernel\Plugin\ReferenceDiscovery;

use Drupal\comment\Entity\Comment as CommentEntity;
use Drupal\eme\EmeObject;
use Drupal\eme\Plugin\Eme\ReferenceDiscovery\Comment;
use Drupal\eme\Plugin\Eme\ReferenceDiscovery\CommentEntityStatistics;

/**
 * Tests the comment reference discovery plugin.
 *
 * @group eme
 */
class CommentTest extends ReferenceDiscoveryPluginTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'comment',
    'field',
    'filter',
    'system',
    'text',
  ];

  /**
   * Tests that comment dependencies related to an entity are discovered.
   *
   * @covers \Drupal\eme\Plugin\Eme\ReferenceDiscovery\Comment::fetchReverseReferences
   * @covers \Drupal\eme\Plugin\Eme\ReferenceDiscovery\CommentEntityStatistics::fetchReverseReferences
   *
   * @dataProvider providerTestFetchReverseReferences
   */
  public function testFetchReverseReferences(array $entityCommentIds, array $otherEntityCommentIds): void {
    foreach ($entityCommentIds as $cid) {
      $this->createComment($cid, $this->entity->id(), $this->entity->getEntityTypeId());
    }
    foreach ($otherEntityCommentIds as $cid) {
      $this->createComment($cid, $this->otherEntity->id(), $this->otherEntity->getEntityTypeId());
    }

    $plugin = new Comment(
      [],
      'comment',
      [],
      $this->container->get('entity_type.manager'),
      $this->container->get('entity_field.manager'),
    );

    $this->assertEquals(
      array_map(
        fn (string|int $id): EmeObject => new EmeObject('comment', [$id]),
        $entityCommentIds,
      ),
      $plugin->fetchReverseReferences(
        new EmeObject($this->entity->getEntityTypeId(), [$this->entity->id()])
      ),
    );

    // Comment statistics is expected to be present, because having a record for
    // a comment field containing 0 comments is also counts.
    $statisticsPlugin = new CommentEntityStatistics(
      [],
      'comment',
      [],
      $this->container->get('entity_type.manager'),
      $this->container->get('entity_field.manager'),
      $this->container->get('comment.statistics'),
    );

    $this->assertEquals(
      [
        new EmeObject('comment_entity_statistics', [
          $this->entity->id(),
          $this->entity->getEntityTypeId(),
          static::COMMENT_FIELD,
        ]),
      ],
      $statisticsPlugin->fetchReverseReferences(
        new EmeObject($this->entity->getEntityTypeId(), [$this->entity->id()])
      ),
    );
  }

  /**
   * Data provider for ::testFetchReverseReferences.
   *
   * @return array
   *   The test cases.
   */
  public static function providerTestFetchReverseReferences(): array {
    return [
      'No comments' => [
        'entityCommentIds' => [],
        'otherEntityCommentIds' => [],
      ],
      'Menu links for test entity' => [
        'entityCommentIds' => [1, 3, 5],
        'otherEntityCommentIds' => [],
      ],
      'Menu links for other test entity' => [
        'entityCommentIds' => [],
        'otherEntityCommentIds' => [2],
      ],
      'Menu links for test entity and for other entities' => [
        'entityCommentIds' => [2],
        'otherEntityCommentIds' => [1, 3, 5],
      ],
    ];
  }

  /**
   * Creates a comment.
   *
   * @param string|int $cid
   *   The ID of the comment.
   * @param string $hostEntityTypeId
   *   The entity type ID of the host entity the comment belongs to.
   * @param string|int $hostEntityId
   *   The ID of the host entity the comment belongs to.
   */
  protected function createComment(string|int $cid, string $hostEntityTypeId, string|int $hostEntityId): void {
    CommentEntity::create([
      'status' => 1,
      'cid' => $cid,
      'entity_id' => $hostEntityTypeId,
      'entity_type' => $hostEntityId,
      'field_name' => static::COMMENT_FIELD,
      'comment_type' => static::COMMENT_TYPE,
      'subject' => $this->randomString(),
      'thread' => '00/',
      'comment_body' => [
        'value' => $this->randomString(),
        'format' => 'plain_text',
      ],
    ])->save();
  }

}
