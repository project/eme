/**
 * @file
 * Controls bundle-specific checkboxes on the EME export form.
 */

(function ($, Drupal, once) {
  Drupal.behaviors.emeAllOrSpecific = {
    attach: function attach(context, settings) {
      once('eme-all-or-specific', $('input[type="checkbox"]'), context).forEach(
        function (element) {
          $(element).on('change', function (event) {
            if (!event.target.checked) {
              return;
            }
            const value = event.target.value;
            if (value.indexOf(':') > 0) {
              // Subvalue, so the main checkbox must be unchecked.
              $(`input[type="checkbox"][value="${value.split(':')[0]}"]`).prop(
                'checked',
                false,
              );
            } else {
              // Main value, so all subvalues must be unchecked.
              $(`input[type="checkbox"][value^="${value}:"]`).prop(
                'checked',
                false,
              );
            }
          });
        },
      );
    },
  };
})(jQuery, Drupal, once);
